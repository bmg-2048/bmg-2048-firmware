#include "mc_level.h"

bool mc_rcc_init() {
    // By default:
    // PLL SYS: 12 / 1 = 12MHz * 125 = 1500MHz / 6 / 2 = 125MHz
    // PLL USB: 12 / 1 = 12MHz * 100 = 1200MHz / 5 / 5 =  48MHz
    // CLK_REF = XOSC (12MHz) / 1 = 12MHz
    // CLK SYS = PLL SYS (125MHz) / 1 = 125MHz
    // CLK USB = PLL USB (48MHz) / 1 = 48MHz
    // CLK ADC = PLL USB (48MHZ) / 1 = 48MHz
    // CLK RTC = PLL USB (48MHz) / 1024 = 46875Hz
    // CLK PERI = clk_sys. Used as reference clock for Peripherals. No dividers
    // so just select and enable Normally choose clk_sys or clk_usb
    return true;
}
