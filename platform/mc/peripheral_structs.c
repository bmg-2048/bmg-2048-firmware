#include "mc_level.h"

bool mc_peripheral_init() {
  if (!mc_rcc_init()) {
    return false;
  }

  if (!mc_gpio_init()) {
    return false;
  }

  if (!mc_adc_init()) {
    return false;
  }

  if (!mc_spi_init()) {
    return false;
  }

  return true;
}
