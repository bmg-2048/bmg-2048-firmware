#include "hardware/adc.h"
#include "pico/stdlib.h"

bool mc_adc_init() {
    adc_init();
    return true;
}

bool mc_adc_start() {
    return true;
}

uint16_t mc_adc_x_get() {
    adc_select_input(0);
    return 4095 - adc_read();
}

uint16_t mc_adc_y_get() {
    adc_select_input(1);
    return 4095 - adc_read();
}
