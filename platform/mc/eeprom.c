#include "pico/stdlib.h"

void mc_eeprom_update() {
 // eeprom_update();
}

bool mc_eeprom_is_busy(bool *busy) {
  //return eeprom_is_busy(busy);
  return true;
}

bool mc_eeprom_word_start_to_write(uint8_t eeprom_word_num,
                                   uint32_t data) {
 // return eeprom_word_start_to_write(eeprom_word_num, data);
    return true;
}

bool mc_eeprom_array_start_to_write(uint8_t eeprom_word_num,
                                    uint32_t *data,
                                    uint8_t data_word_number) {
  /*return eeprom_array_start_to_write(eeprom_word_num, data,
                                     data_word_number);*/
    return true;
}

bool mc_eeprom_word_read(uint8_t eeprom_word_num,
                         uint32_t *ret_data) {
 // return eeprom_word_read(eeprom_word_num, ret_data);
    return true;
}

bool mc_eeprom_array_read(uint8_t eeprom_word_num,
                          uint8_t data_word_number,
                          uint32_t *ret_buf) {
  /*return eeprom_array_read(eeprom_word_num,
                           data_word_number, ret_buf);*/
    return true;
}
