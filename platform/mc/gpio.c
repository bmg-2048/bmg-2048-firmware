#include "hardware/adc.h"
#include "pico/binary_info.h"
#include "pico/stdlib.h"

#define PIN_LED 25

#define PIN_SPI_SCK  18
#define PIN_SPI_MOSI 19
#define PIN_SPI_MISO 16

#define PIN_LCD_CS 22
#define PIN_LCD_A0 0

#define PIN_IN_BUTTONS_RIGHT 1
#define PIN_IN_BUTTONS_LEFT  3
#define PIN_IN_BUTTONS_UP    4
#define PIN_IN_BUTTONS_DOWN  2
#define PIN_IN_BUTTONS_STICK 5

#define PIN_ADC_X 26
#define PIN_ADC_Y 27

/*
// ADC.
#define GPIO_ADC     GPIO_NUMBER_A
#define PINS_ADC_MSK (GPIO_PIN_MSK_6 | GPIO_PIN_MSK_7)

static const gpio_pins_cfg adc_pins_cfg = {.gpio     = GPIO_ADC,
  .pins     = PINS_ADC_MSK,
  .pin_mode = GPIO_PIN_MODE_ANALOG,
  .speed    = GPIO_SPEED_LOW,
  .pull     = GPIO_PULL_NO,
  .alt_func = GPIO_ALT_FUNC_NO,
  .out_mode = GPIO_OUT_MODE_NO,
  .it_mode  = GPIO_IT_MODE_NO
};
*/
bool mc_lcd_cs_pin_set(bool state) {
    gpio_put(PIN_LCD_CS, state);
    return true;
}

bool mc_lcd_a0_pin_set(bool state) {
    gpio_put(PIN_LCD_A0, state);
    return true;
}

bool mc_led_pin_set(bool state) {
    gpio_put(PIN_LED, state);
    return true;
}

bool mc_button_pin_right_state_get(bool* ret_state) {
    *ret_state = !gpio_get(PIN_IN_BUTTONS_RIGHT);
    return true;
}

bool mc_button_pin_left_state_get(bool* ret_state) {
    *ret_state = !gpio_get(PIN_IN_BUTTONS_LEFT);
    return true;
}

bool mc_button_pin_up_state_get(bool* ret_state) {
    *ret_state = !gpio_get(PIN_IN_BUTTONS_UP);
    return true;
}

bool mc_button_pin_down_state_get(bool* ret_state) {
    *ret_state = !gpio_get(PIN_IN_BUTTONS_DOWN);
    return true;
}

bool mc_button_pin_stick_state_get(bool* ret_state) {
    *ret_state = !gpio_get(PIN_IN_BUTTONS_STICK);
    return true;
}

bool mc_gpio_init() {
    // LED.
    gpio_init(PIN_LED);
    gpio_set_dir(PIN_LED, GPIO_OUT);

    // SPI.
    gpio_set_function(PIN_SPI_SCK, GPIO_FUNC_SPI);
    gpio_set_function(PIN_SPI_MOSI, GPIO_FUNC_SPI);
    gpio_set_function(PIN_SPI_MISO, GPIO_FUNC_SPI);

    // Make the SPI pins available to picotool
    bi_decl(bi_3pins_with_func(PIN_SPI_SCK, PIN_SPI_MOSI, PIN_SPI_MISO,
                               GPIO_FUNC_SPI));
    // LCD.
    gpio_init(PIN_LCD_CS);
    gpio_set_dir(PIN_LCD_CS, GPIO_OUT);
    gpio_put(PIN_LCD_CS, true);

    gpio_init(PIN_LCD_A0);
    gpio_set_dir(PIN_LCD_A0, GPIO_OUT);

    gpio_init(PIN_IN_BUTTONS_RIGHT);
    gpio_set_dir(PIN_IN_BUTTONS_RIGHT, GPIO_IN);

    gpio_init(PIN_IN_BUTTONS_LEFT);
    gpio_set_dir(PIN_IN_BUTTONS_LEFT, GPIO_IN);

    gpio_init(PIN_IN_BUTTONS_UP);
    gpio_set_dir(PIN_IN_BUTTONS_UP, GPIO_IN);

    gpio_init(PIN_IN_BUTTONS_DOWN);
    gpio_set_dir(PIN_IN_BUTTONS_DOWN, GPIO_IN);

    gpio_init(PIN_IN_BUTTONS_STICK);
    gpio_set_dir(PIN_IN_BUTTONS_STICK, GPIO_IN);

    adc_gpio_init(PIN_ADC_X);
    adc_gpio_init(PIN_ADC_Y);

    return true;
}
