#include "hardware/spi.h"
#include "event.h"
#include "hardware/dma.h"
#include "mc_level.h"

bool mc_spi_init() {
    spi_init(spi_default, 1000 * 1000);
    return true;
}

void mc_spi_update(uint16_t event) {
    (void)event;
}

bool mc_spi_is_busy() {
    return false;
}

bool mc_spi_trans_start(const uint8_t* tx, uint8_t* rx, uint16_t len) {
    spi_write_read_blocking(spi_default, tx, rx, len);
    return true;
}

bool mc_spi_tx_start(const uint8_t* tx, uint16_t len) {
    spi_write_blocking(spi_default, tx, len);
    return true;
}
