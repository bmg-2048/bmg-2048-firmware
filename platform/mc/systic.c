#include "mc_level.h"

#include "event.h"
#include "hardware/structs/systick.h"

bool mc_systic_start() {
    systick_hw->csr = 0;  // Disable
    systick_hw->rvr =
        124999UL;  // Standard System clock (125Mhz)/ (rvr value + 1) = 1ms
    systick_hw->cvr = 0;    // clear the count to force initial reload
    systick_hw->csr = 0x7;  // Enable Systic, Enable Exceptions
    return true;
}

static volatile uint64_t time = 0;

void isr_systick() {
    time++;
    event_add(EVENT_SYSTIC);
}

uint64_t mc_systick_counter_get() {
    return time;
}
