#pragma once

#include "pico/stdlib.h"

/**
 *
 */
bool mc_rcc_init();

/**
 *
 */
bool mc_gpio_init();

/**
 *
 */
bool mc_adc_init();

/**
 *
 */
bool mc_spi_init();

/**
 *
 */
bool mc_peripheral_init();

/**
 *
 */
bool mc_led_pin_set(bool state);

/**
 *
 */
bool mc_lcd_cs_pin_set(bool state);

/**
 *
 */
bool mc_lcd_a0_pin_set(bool state);

/**
 *
 */
bool mc_button_pin_right_state_get(bool *ret_state);

/**
 *
 */
bool mc_button_pin_left_state_get(bool *ret_state);

/**
 *
 */
bool mc_button_pin_up_state_get(bool *ret_state);

/**
 *
 */
bool mc_button_pin_down_state_get(bool *ret_state);

/*
 *
 */
bool mc_button_pin_stick_state_get(bool *ret_state);

/**
 *
 */
bool mc_adc_start();

/**
 *
 */
uint16_t mc_adc_x_get();

/**
 *
 */
uint16_t mc_adc_y_get();

/**
 *
 */
bool mc_spi_is_busy();

/**
 *
 */
bool mc_spi_trans_start(const uint8_t *tx, uint8_t *rx,
                        uint16_t len);

/**
 *
 */
bool mc_spi_tx_start(const uint8_t *tx, uint16_t len);

/**
 *
 */
bool mc_systic_start();

/**
 *
 */
void mc_spi_update(uint16_t event);

/**
 *
 */
uint64_t mc_systick_counter_get();

/**
 *
 */
void mc_eeprom_update();

/**
 *
 */
bool mc_eeprom_is_busy(bool *busy);

/**
 *
 */
bool mc_eeprom_word_start_to_write(uint8_t eeprom_word_num,
                                   uint32_t data);

/**
 *
 */
bool mc_eeprom_array_start_to_write(uint8_t eeprom_word_num,
                                    uint32_t *data,
                                    uint8_t data_word_number);

/**
 *
 */
bool mc_eeprom_word_read(uint8_t eeprom_word_num,
                         uint32_t *ret_data);

/**
 *
 */
bool mc_eeprom_array_read(uint8_t eeprom_word_num,
                          uint8_t data_word_number,
                          uint32_t *ret_buf);

/**
 *
 */
void mc_crc_reset();

/**
 *
 */
void mc_crc_item_add(uint32_t item);

/*
 *
 */
uint32_t mc_crc_result_get();
