#include <stdint.h>

#define U8(val)  ((uint8_t)(val))
#define U16(val) ((uint16_t)(val))
#define U32(val) ((uint32_t)(val))

#define U8P(val)  ((uint8_t*)(val))
#define U16P(val) ((uint16_t*)(val))
#define U32P(val) ((uint32_t*)(val))

#define U8M(bit)  (U8(U8(1u) << U8(bit)))
#define U16M(bit) (U16(U16(1u) << U8(bit)))
#define U32M(bit) (U32(U32(1u) << U8(bit)))
