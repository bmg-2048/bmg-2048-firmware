#pragma once

#define B_RIGHT 0
#define B_LEFT  1
#define B_UP    2
#define B_DOWN  3
#define B_JOY   4
#define B_NO    5

#define EVENT_QUEUE_SIZE 10
#define CPU_CLK          CPU_FREQ_16_MHZ
#define MS_TO_TICK(MS)   (MS / 10)
