#include "event.h"

static uint16_t queue[EVENT_QUEUE_SIZE] = {0};
static int16_t  queue_top_item          = -1;

void event_add(uint16_t event) {
    all_irq_disable();
    if (queue_top_item + 1 >= EVENT_QUEUE_SIZE) {
        reboot();
        return;
    } else {
        queue_top_item++;
    }
    queue[queue_top_item] = event;
    all_irq_enable();
}

uint16_t event_get() {
    all_irq_disable();
    if (queue_top_item == -1) {
        all_irq_enable();
        return EVENT_NO;
    }
    uint16_t event = queue[queue_top_item];
    queue_top_item--;
    all_irq_enable();
    return event;
}

void event_queue_reset() {
    queue_top_item = -1;
}
