#pragma once

#include <stdbool.h>
#include <stdint.h>

#ifndef EVENT_QUEUE_SIZE
#define EVENT_QUEUE_SIZE 10
#endif

enum EVENT { EVENT_NO = 0, EVENT_SYSTIC = 1, EVENT_SPI_COMPLITE = 2 };

/**
 * \brief Wait in timeout in switch-thread.
 */
#define DELAY()                  \
    if (event != EVENT_SYSTIC) { \
        break;                   \
    }                            \
    cfg->ram->wait--;            \
    if (cfg->ram->wait != 0) {   \
        break;                   \
    }

/**
 * \brief Add irq event to queue.
 * \param event[in] Adding irq event (Only IRQ_QUEUE_EVENT).
 */
void event_add(uint16_t event);

/**
 * \brief Get irq event (call with irq_disable).
 */
uint16_t event_get();

/**
 * \brief Reset the event queue.
 */
void event_queue_reset();

// External functions.
void all_irq_disable();
void all_irq_enable();
void reboot();
