#include "app_level.h"
#include "event.h"
#include "mc_level.h"
#include "pcb_level.h"
#include "service_level.h"


int main() {
    if (!mc_peripheral_init()) {
        return -1;
    }

    if (!mc_adc_start()) {
        return -1;
    }

    if (!mc_systic_start()) {
        return -1;
    }

    while (true) {
        uint16_t event = event_get();
        mc_spi_update(event);
        mc_eeprom_update();
        if (!pcb_lcd_update(event)) {
            while (true)
                ;
        }
        if (!pcb_action_joystick_update(event)) {
            while (true)
                ;
        }
        if (!pcb_check_buttons_update(event)) {
            while (true)
                ;
        }
        if (!service_led_blink_update(event)) {
            while (true)
                ;
        }
        if (!service_app_cfg_controller_update()) {
            while (true)
                ;
        }
        app_app_controller_update(event);
    }
}
