#include "lcd_st7565.h"
#include "char_font_5x7_stolb_y_inv.h"
#include "number_font_3x5_stolb_y_inv.h"

#include <string.h>

bool lcd_st7565_is_busy(const lcd_st7565_cfg* cfg, bool* busy) {
    if (!cfg) {
        return false;
    }
    *busy = cfg->ram->busy;
    return true;
}

#define STEP_WAIT_SPI_END 1
#define STEP_SEND_CMD     2
#define STEP_RESET        3
#define STEP_UPDATE       6

#define CMD_DISPLAY_OFF 0xAE
#define CMD_DISPLAY_ON  0xAF

#define CMD_SET_DISP_START_LINE 0x40
#define CMD_SET_PAGE            0xB0

#define CMD_SET_COLUMN_UPPER 0x10
#define CMD_SET_COLUMN_LOWER 0x00

#define CMD_SET_ADC_NORMAL  0xA0
#define CMD_SET_ADC_REVERSE 0xA1

#define CMD_SET_DISP_NORMAL  0xA6
#define CMD_SET_DISP_REVERSE 0xA7

#define CMD_SET_ALLPTS_NORMAL 0xA4
#define CMD_SET_ALLPTS_ON     0xA5
#define CMD_SET_BIAS_9        0xA2
#define CMD_SET_BIAS_7        0xA3

#define CMD_RMW                0xE0
#define CMD_RMW_CLEAR          0xEE
#define CMD_INTERNAL_RESET     0xE2
#define CMD_SET_COM_NORMAL     0xC0
#define CMD_SET_COM_REVERSE    0xC8
#define CMD_SET_POWER_CONTROL  0x28
#define CMD_SET_RESISTOR_RATIO 0x20
#define CMD_SET_VOLUME_FIRST   0x81
#define CMD_SET_VOLUME_SECOND  0
#define CMD_SET_STATIC_OFF     0xAC
#define CMD_SET_STATIC_ON      0xAD
#define CMD_SET_STATIC_REG     0x0
#define CMD_SET_BOOSTER_FIRST  0xF8
#define CMD_SET_BOOSTER_234    0
#define CMD_SET_BOOSTER_5      1
#define CMD_SET_BOOSTER_6      3
#define CMD_NOP                0xE3
#define CMD_TEST               0xF0

#define WAIT_FLAG 0xFF

uint8_t init_cmds[14] = {
    CMD_SET_BIAS_9,      // LCD bias select
    CMD_SET_ADC_NORMAL,  // ADC select
    CMD_SET_DISP_NORMAL,
    CMD_SET_COM_NORMAL,       // SHL select
    CMD_SET_DISP_START_LINE,  // Initial display line
    CMD_SET_POWER_CONTROL |
        0x4,    // turn on voltage converter (VC=1, VR=0, VF=0)
    WAIT_FLAG,  // Wait 50 ms.
    CMD_SET_POWER_CONTROL |
        0x6,    // turn on voltage regulator (VC=1, VR=1, VF=0)
    WAIT_FLAG,  // Wait 50 ms.
    CMD_SET_POWER_CONTROL | 0x7,  // turn on voltage follower (VC=1, VR=1, VF=1)
    WAIT_FLAG,                    // Wait 50 ms.
    CMD_SET_RESISTOR_RATIO | 0x6,  // set lcd operating voltage (regulator
    // resistor, ref voltage resistor)
    CMD_SET_ALLPTS_NORMAL,
    CMD_SET_VOLUME_SECOND | 0x24};  // 0x24 is experemental contrast.

static const uint8_t column_upper_cmd[2] = {CMD_SET_COLUMN_UPPER, 4};

bool lcd_st7565_update(const lcd_st7565_cfg* cfg, uint16_t event) {
    if (!cfg) {
        return false;
    }

    if (cfg->ram->step == 0) {
        return true;
    }

    switch (cfg->ram->step) {
        case STEP_WAIT_SPI_END:
            if (cfg->tx_port_is_bsy()) {
                return true;
            }
            if (!cfg->cs(1)) {
                return false;
            }
            cfg->ram->busy = false;
            cfg->ram->step = 0;
            break;

        case STEP_SEND_CMD:
            if (cfg->tx_port_is_bsy()) {
                return true;
            }
            if (!cfg->a0(0)) {
                return false;
            }
            if (!cfg->cs(0)) {
                return false;
            }
            if (!cfg->data_tx_start(&cfg->ram->cmd, 1)) {
                return false;
            }
            cfg->ram->step = STEP_WAIT_SPI_END;
            break;

        case STEP_RESET:
            if (init_cmds[cfg->ram->cmd] != WAIT_FLAG) {
                if (cfg->tx_port_is_bsy()) {
                    return true;
                }
                if (!cfg->a0(0)) {
                    return false;
                }
                if (!cfg->cs(0)) {
                    return false;
                }
                if (!cfg->data_tx_start(&init_cmds[cfg->ram->cmd], 1)) {
                    return false;
                }
                cfg->ram->step = STEP_RESET + 1;
            } else {
                cfg->ram->wait = MS_TO_TICK(50);
                cfg->ram->step = STEP_RESET + 2;
            }
            cfg->ram->cmd++;
            if (cfg->ram->cmd == sizeof(init_cmds)) {
                cfg->ram->step = STEP_WAIT_SPI_END;
            }
            break;

        case STEP_RESET + 1:
            if (cfg->tx_port_is_bsy()) {
                return true;
            }
            if (!cfg->cs(1)) {
                return false;
            }
            cfg->ram->step = STEP_RESET;
            break;

        case STEP_RESET + 2:
            DELAY()
            cfg->ram->step = STEP_RESET;
            break;

        case STEP_UPDATE:
            if (cfg->tx_port_is_bsy()) {
                return true;
            }
            if (!cfg->cs(1)) {
                return false;
            }
            if (!cfg->a0(0)) {
                return false;
            }
            if (!cfg->cs(0)) {
                return false;
            }
            if (!cfg->data_tx_start(&cfg->ram->cmd, 1)) {
                return false;
            }
            cfg->ram->step++;
            break;

        case STEP_UPDATE + 1:
            if (cfg->tx_port_is_bsy()) {
                return true;
            }
            if (!cfg->cs(1)) {
                return false;
            }
            if (!cfg->cs(0)) {
                return false;
            }
            if (!cfg->data_tx_start(column_upper_cmd, 2)) {
                return false;
            }
            cfg->ram->step++;
            break;

        case STEP_UPDATE + 2:
            if (cfg->tx_port_is_bsy()) {
                return true;
            }
            if (!cfg->cs(1)) {
                return false;
            }
            if (!cfg->a0(1)) {
                return false;
            }
            if (!cfg->cs(0)) {
                return false;
            }

            if (!cfg->data_tx_start(
                    &cfg->ram->img[(7 - (cfg->ram->cmd & (~CMD_SET_PAGE))) *
                                   LCD_WIDTH],
                    LCD_WIDTH)) {
                return false;
            }

            cfg->ram->cmd--;

            if (cfg->ram->cmd != CMD_SET_PAGE - 1) {
                cfg->ram->step = STEP_UPDATE;
            } else {
                cfg->ram->step = STEP_WAIT_SPI_END;
            }

            break;
    }

    return true;
}

bool lcd_st7565_contrast_set(const lcd_st7565_cfg* cfg, uint8_t val) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }
    cfg->ram->step = STEP_SEND_CMD;
    cfg->ram->cmd  = CMD_SET_VOLUME_SECOND | (val & 0x3f);
    cfg->ram->busy = true;
    return true;
}

bool lcd_st7565_on(const lcd_st7565_cfg* cfg) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }
    cfg->ram->step = STEP_SEND_CMD;
    cfg->ram->cmd  = CMD_DISPLAY_ON;
    cfg->ram->busy = true;
    return true;
}

bool lcd_st7565_off(const lcd_st7565_cfg* cfg) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }
    cfg->ram->step = STEP_SEND_CMD;
    cfg->ram->cmd  = CMD_DISPLAY_OFF;
    cfg->ram->busy = true;
    return true;
}

bool lcd_st7565_init(const lcd_st7565_cfg* cfg) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }
    return true;
}

bool lcd_st7565_reset(const lcd_st7565_cfg* cfg) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }
    cfg->ram->step = STEP_RESET;
    cfg->ram->busy = true;
    return true;
}

bool lcd_st7565_clear(const lcd_st7565_cfg* cfg) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }
    memset(cfg->ram->img, 0, sizeof(cfg->ram->img));
    return true;
}

bool lcd_st7565_send_buffer(const lcd_st7565_cfg* cfg) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }
    cfg->ram->cmd  = CMD_SET_PAGE | 7;
    cfg->ram->step = STEP_UPDATE;
    cfg->ram->busy = true;
    return true;
}

bool lcd_st7565_horizontal_line_draw(const lcd_st7565_cfg* cfg, uint8_t x,
                                     uint8_t y, uint8_t len, bool invert) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }

    uint8_t* p = &cfg->ram->img[x + (y / PIXELS_NUMBER_IN_BYTE) * LCD_WIDTH];

    uint8_t msk = (uint8_t)(0x80 >> (y % PIXELS_NUMBER_IN_BYTE));
    do {
        if (invert) {
            *p &= (uint8_t)~msk;
        } else {
            *p |= msk;
        }
        p++;
        len--;
    } while (len);

    return true;
}

bool lcd_st7565_vertical_line_draw(const lcd_st7565_cfg* cfg, uint8_t x,
                                   uint8_t y, uint8_t len, bool invert) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }

    uint8_t* p = &cfg->ram->img[x + (y / PIXELS_NUMBER_IN_BYTE) * LCD_WIDTH];

    if (y % PIXELS_NUMBER_IN_BYTE) {
        uint8_t y_seek_byte    = y % PIXELS_NUMBER_IN_BYTE;
        uint8_t first_byte_msk = (uint8_t)(0xFFu >> y_seek_byte);
        if (len < PIXELS_NUMBER_IN_BYTE - y_seek_byte) {
            while (true)
                ;
        } else {
            if (invert) {
                *p &= (uint8_t)~first_byte_msk;
            } else {
                *p |= first_byte_msk;
            }
            len =
                (uint8_t)(len - (uint8_t)(PIXELS_NUMBER_IN_BYTE - y_seek_byte));
            p += LCD_WIDTH;
        }
    }

    while (len > (PIXELS_NUMBER_IN_BYTE - 1)) {
        if (invert) {
            *p = 0u;
        } else {
            *p = 0xFFu;
        }
        len = (uint8_t)(len - PIXELS_NUMBER_IN_BYTE);
        p += LCD_WIDTH;
    }

    if (len) {
        uint8_t first_byte_msk =
            (uint8_t)(0xFFu << (PIXELS_NUMBER_IN_BYTE - len));
        if (invert) {
            *p &= (uint8_t)~first_byte_msk;
        } else {
            *p |= first_byte_msk;
        }
    }

    return true;
}

bool lcd_st7565_rectangle_draw(const lcd_st7565_cfg* cfg, uint8_t x, uint8_t y,
                               uint8_t width, uint8_t height, bool invert) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }

    if (!lcd_st7565_horizontal_line_draw(cfg, x, y, width,
                                         invert)) {  // - up
        return false;
    }

    if (!lcd_st7565_horizontal_line_draw(
            cfg, x, (uint8_t)((uint8_t)(y + height) - 1u), width,
            invert)) {  // _ down
        return false;
    }

    if (!lcd_st7565_vertical_line_draw(cfg, x, y, height,
                                       invert)) {  // |
        return false;
    }

    if (!lcd_st7565_vertical_line_draw(
            cfg, (uint8_t)((uint8_t)(x + width) - 1u), y, height,
            invert)) {  // | right
        return false;
    }

    return true;
}

bool lcd_st7565_rectangle_full_draw(const lcd_st7565_cfg* cfg, uint8_t x,
                                    uint8_t y, uint8_t width, uint8_t height,
                                    bool invert) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }

    do {
        if (!lcd_st7565_horizontal_line_draw(cfg, x, y, width, invert)) {
            return false;
        }
        y++;
        height--;
    } while (height);

    return true;
}

bool lcd_st7565_small_one_number_print(const lcd_st7565_cfg* cfg, uint8_t x,
                                       uint8_t y, uint8_t val, bool invert) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }

    const uint8_t* img = &number_font_3x5_stolb_y_inv[val][0];
    uint8_t* p = &cfg->ram->img[x + (y / PIXELS_NUMBER_IN_BYTE) * LCD_WIDTH];


    for (uint8_t cal_loop = 0; cal_loop < 3; cal_loop++) {
        uint8_t str = *img++;

        uint8_t y_seek_byte = y % 8;
        if (y_seek_byte == 0) {
            if (invert) {
                *p &= (uint8_t)~str;
            } else {
                *p |= str;
            }
        } else {
            if (invert) {
                p[0] &= (uint8_t) ~(str >> y_seek_byte);
                p[LCD_WIDTH] &= (uint8_t) ~(str << (8 - y_seek_byte));
            } else {
                p[0] |= (uint8_t)(str >> y_seek_byte);
                p[LCD_WIDTH] |= (uint8_t)(str << (8 - y_seek_byte));
            }
        }

        p++;
    }

    return true;
}

bool lcd_st7565_char_print(const lcd_st7565_cfg* cfg, uint8_t x, uint8_t y,
                           char ch, bool invert) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }

    if (ch == ' ') {
        return true;
    }

    const uint8_t* img =
        &char_font_5x7_stolb_y_inv[(uint8_t)ch - (uint8_t)'0'][0];
    uint8_t* p = &cfg->ram->img[x + (y / PIXELS_NUMBER_IN_BYTE) * LCD_WIDTH];


    for (uint8_t cal_loop = 0; cal_loop < 5; cal_loop++) {
        uint8_t str = *img++;

        uint8_t y_seek_byte = y % PIXELS_NUMBER_IN_BYTE;
        if (y_seek_byte == 0) {
            if (invert) {
                *p &= (uint8_t)~str;
            } else {
                *p |= str;
            }
        } else {
            if (invert) {
                p[0] &= (uint8_t) ~(str >> y_seek_byte);
                p[LCD_WIDTH] &= (uint8_t) ~(str << (8 - y_seek_byte));
            } else {
                p[0] |= (uint8_t)(str >> y_seek_byte);
                p[LCD_WIDTH] |= (uint8_t)(str << (8 - y_seek_byte));
            }
        }

        p++;
    }

    return true;
}

bool lcd_st7565_string_print(const lcd_st7565_cfg* cfg, uint8_t x, uint8_t y,
                             const char* str, bool invert) {
    if ((!cfg) || (cfg->ram->busy) || !str) {
        return false;
    }

    while (*str != 0) {
        if (!lcd_st7565_char_print(cfg, x, y, *str++, invert)) {
            return false;
        }
        x = (uint8_t)(x + 6);
    }
    return true;
}

bool lcd_st7565_dec_small_number_print(const lcd_st7565_cfg* cfg, uint8_t x,
                                       uint8_t y, uint32_t val, bool invert) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }

    uint8_t  num     = 1;
    uint32_t check_v = 10;
    while (true) {
        if (val < check_v) {
            break;
        }
        num++;
        check_v = (uint32_t)(check_v * 10);
    }

    check_v /= 10;

    for (uint32_t i = 0; i < num; i++) {
        uint32_t print_value = (val / check_v);
        val -= print_value * check_v;
        check_v /= 10;

        if (!lcd_st7565_small_one_number_print(cfg, x, y, (uint8_t)print_value,
                                               invert)) {
            return false;
        }
        x = (uint8_t)(x + 4u);
    }

    return true;
}

bool lcd_st7565_dec_small_number_to_center_print(const lcd_st7565_cfg* cfg,
                                                 uint8_t x_start, uint8_t x_end,
                                                 uint8_t y, uint32_t val,
                                                 bool invert) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }

    uint8_t  num     = 1;
    uint32_t check_v = 10;
    while (true) {
        if (val < check_v) {
            break;
        }
        num++;
        check_v *= 10;
    }

    uint8_t len        = (uint8_t)((uint8_t)(num * 4u) - 1u);
    uint8_t full_width = (uint8_t)((uint8_t)(x_end - x_start) + 1u);
    return lcd_st7565_dec_small_number_print(
        cfg, (uint8_t)((uint8_t)(x_start + (full_width - len) / 2u)), y, val,
        invert);
}

bool lcd_st7565_dec_number_print(const lcd_st7565_cfg* cfg, uint8_t x,
                                 uint8_t y, uint32_t val, bool invert) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }

    uint8_t  num     = 1;
    uint32_t check_v = 10;
    while (true) {
        if (val < check_v) {
            break;
        }
        num++;
        check_v *= 10;
    }

    check_v /= 10;

    for (uint32_t i = 0; i < num; i++) {
        uint32_t print_value = (val / check_v);
        val -= print_value * check_v;
        check_v /= 10;

        if (!lcd_st7565_char_print(
                cfg, x, y, (uint8_t)((uint8_t)print_value + (uint8_t)'0'),
                invert)) {
            return false;
        }
        x = (uint8_t)(x + 6u);
    }

    return true;
}

bool lcd_st7565_hex_small_number_print(const lcd_st7565_cfg* cfg, uint8_t x,
                                       uint8_t y, uint32_t val, uint8_t num_ch,
                                       bool invert) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }

    if ((num_ch > 8) || (num_ch == 0)) {
        return false;
    }

    uint32_t msk = 0xF0000000;
    val <<= 4 * (8 - num_ch);
    for (uint32_t i = 0; i < num_ch; i++) {
        uint8_t print_value = (uint8_t)((val & msk) >> (4 * 7));
        if (!lcd_st7565_small_one_number_print(cfg, x, y, print_value,
                                               invert)) {
            return false;
        }
        val <<= 4;
        x = (uint8_t)(x + 4u);
    }
    return true;
}

bool lcd_st7565_hex_number_print(const lcd_st7565_cfg* cfg, uint8_t x,
                                 uint8_t y, uint32_t val, uint8_t num_ch,
                                 bool invert) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }

    if ((num_ch > 8) || (num_ch == 0)) {
        return false;
    }

    uint32_t msk = 0xF0000000;
    val <<= 4 * (8 - num_ch);

    for (uint32_t i = 0; i < num_ch; i++) {
        uint8_t print_value = (uint8_t)((val & msk) >> (4 * 7));
        if (print_value < 10) {
            if (!lcd_st7565_char_print(
                    cfg, x, y, (uint8_t)(print_value + (uint8_t)'0'), invert)) {
                return false;
            }
        } else {
            if (!lcd_st7565_char_print(
                    cfg, x, y, (uint8_t)((print_value - 10) + (uint8_t)'A'),
                    invert)) {
                return false;
            }
        }
        val <<= 4;
        x = (uint8_t)(x + 6u);
    }
    return true;
}

bool lcd_st7565_string_to_center_print(const lcd_st7565_cfg* cfg,
                                       uint8_t x_start, uint8_t x_end,
                                       uint8_t y, const char* str,
                                       bool invert) {
    if ((!cfg) || (cfg->ram->busy) || !str) {
        return false;
    }

    uint8_t len        = (uint8_t)((uint8_t)((uint8_t)strlen(str) * 6u) - 1u);
    uint8_t full_width = (uint8_t)((uint8_t)(x_end - x_start) + 1u);
    return lcd_st7565_string_print(
        cfg, (uint8_t)((uint8_t)(x_start + (full_width - len)) / 2u), y, str,
        invert);
}
