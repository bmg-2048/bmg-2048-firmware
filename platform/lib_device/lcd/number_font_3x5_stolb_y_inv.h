#pragma once

#include <stdint.h>

#define CHAR_SIZE_BYTES 3
#define CHAR_NUMBER     16
static const uint8_t number_font_3x5_stolb_y_inv[CHAR_NUMBER][CHAR_SIZE_BYTES] =
    {{0xf8, 0x88, 0xf8}, {0x48, 0xf8, 0x8},  {0xc8, 0x98, 0xe8},
     {0xa8, 0xa8, 0xf8}, {0xe0, 0x20, 0xf8}, {0xe8, 0xa8, 0xb8},
     {0x78, 0xa8, 0xb8}, {0x80, 0xb8, 0xc0}, {0xf8, 0xa8, 0xf8},
     {0xe8, 0xa8, 0xf8}, {0x78, 0xa0, 0xf8}, {0xf8, 0xa8, 0x78},
     {0x70, 0x88, 0x88}, {0xf8, 0x88, 0x70}, {0xf8, 0xa8, 0xa8},
     {0xf8, 0xa0, 0xa0}};
