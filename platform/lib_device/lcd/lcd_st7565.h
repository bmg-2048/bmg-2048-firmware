#pragma once

#include "app_cfg.h"
#include "event.h"

typedef bool (*lcd_st7565_pin_set)(bool state);
typedef bool (*lcd_st7565_data_tx_start)(const uint8_t* tx, uint16_t len);
typedef bool (*lcd_st7565_data_tx_port_is_bsy)();

#define LCD_HEIGHT            64
#define LCD_WIDTH             128
#define PIXELS_NUMBER_IN_BYTE 8

typedef struct {
    uint8_t img[(LCD_WIDTH * LCD_HEIGHT) / PIXELS_NUMBER_IN_BYTE];
    bool    busy;
    uint8_t step;
    uint8_t wait;
    uint8_t cmd;
} lcd_st7565_ram;

typedef struct {
    lcd_st7565_pin_set             cs;
    lcd_st7565_pin_set             a0;
    lcd_st7565_data_tx_start       data_tx_start;
    lcd_st7565_data_tx_port_is_bsy tx_port_is_bsy;
    lcd_st7565_ram*                ram;
} lcd_st7565_cfg;

/**
 * \brief Return busy state of LCD.
 * \param cfg[in] Config of object.
 * \param busy[out] Current busy state of lcd.
 * \return True if cfg is correct.
 */
bool lcd_st7565_is_busy(const lcd_st7565_cfg* cfg, bool* busy);

/**
 * \brief Init internal data in RAM.
 * \param cfg[in] Config of object.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7565_init(const lcd_st7565_cfg* cfg);

/**
 * \brief Update FSM action.
 * \param cfg[in] Config of object.
 * \param event[in] Current event.
 * \return True if cfg is correct and function from lcd_st7565_cfg returns true.
 */
bool lcd_st7565_update(const lcd_st7565_cfg* cfg, uint16_t event);

/**
 * \brief Start reset operation (hardware pin toggle).
 * \param cfg[in] Config of object.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7565_reset(const lcd_st7565_cfg* cfg);

/**
 * \brief Start to send cmd to enable of lcd graphics.
 * \param cfg[in] Config of object.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7565_on(const lcd_st7565_cfg* cfg);

/**
 * \brief Start to send cmd to disable of lcd graphics.
 * \param cfg[in] Config of object.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7565_off(const lcd_st7565_cfg* cfg);

/**
 * \brief Start to send cmd to set contrast of LCD.
 * \param cfg[in] Config of object.
 * \param val[in] Contrast value.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7565_contrast_set(const lcd_st7565_cfg* cfg, uint8_t val);

/**
 * \brief Clear internal graphics buffer in RAM.
 * \param cfg[in] Config of object.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7565_clear(const lcd_st7565_cfg* cfg);

/**
 * \brief Start to send graphics buffer to real LCD.
 * \param cfg[in] Config of object.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7565_send_buffer(const lcd_st7565_cfg* cfg);

/**
 * \brief Draw horizontal line.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param len[in] Len of line (1..128). Depending on x.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_horizontal_line_draw(const lcd_st7565_cfg* cfg, uint8_t x,
                                     uint8_t y, uint8_t len, bool invert);

/**
 * \brief Draw vertical line.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param len[in] Len of line (1..64). Depending on y.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_vertical_line_draw(const lcd_st7565_cfg* cfg, uint8_t x,
                                   uint8_t y, uint8_t len, bool invert);

/**
 * \brief Draw rectangle.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param width[in] Wdth of rectangle (1..128). Depending on x.
 * \param height[in] Height of rectangle (1..64). Depending on y.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_rectangle_draw(const lcd_st7565_cfg* cfg, uint8_t x, uint8_t y,
                               uint8_t width, uint8_t height, bool invert);

/**
 * \brief Draw rectangle full.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param width[in] Wdth of rectangle (1..128). Depending on x.
 * \param height[in] Height of rectangle (1..64). Depending on y.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_rectangle_full_draw(const lcd_st7565_cfg* cfg, uint8_t x,
                                    uint8_t y, uint8_t width, uint8_t height,
                                    bool invert);
/**
 * \brief Print number.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param val[in] Value (0..F).
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_small_one_number_print(const lcd_st7565_cfg* cfg, uint8_t x,
                                       uint8_t y, uint8_t val, bool invert);

/**
 * \brief Print number.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param ch[in] Char.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_char_print(const lcd_st7565_cfg* cfg, uint8_t x, uint8_t y,
                           char ch, bool invert);

/**
 * \brief Print string.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param str[in] String for printing.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_string_print(const lcd_st7565_cfg* cfg, uint8_t x, uint8_t y,
                             const char* str, bool invert);

/**
 * \brief Print hex number. Small font.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param val[in] Value for printing.
 * \param num_ch[in] Number chars for printing.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_hex_small_number_print(const lcd_st7565_cfg* cfg, uint8_t x,
                                       uint8_t y, uint32_t val, uint8_t num_ch,
                                       bool invert);

/**
 * \brief Print dec number.Small font.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param val[in] Value for printing.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_dec_small_number_print(const lcd_st7565_cfg* cfg, uint8_t x,
                                       uint8_t y, uint32_t val, bool invert);

/**
 * \brief Print dec number to center. Small font.
 * \param cfg[in] Config of object.
 * \param x_start[in] Start border x pos (0..127).
 * \param x_end[in] Stop border x pos (0..127) (> x_start).
 * \param y[in] Start y pos (0..63).
 * \param val[in] Value for printing.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_dec_small_number_to_center_print(const lcd_st7565_cfg* cfg,
                                                 uint8_t x_start, uint8_t x_end,
                                                 uint8_t y, uint32_t val,
                                                 bool invert);

/**
 * \brief Print hex number.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param val[in] Value for printing.
 * \param num_ch[in] Number chars for printing.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_hex_number_print(const lcd_st7565_cfg* cfg, uint8_t x,
                                 uint8_t y, uint32_t val, uint8_t num_ch,
                                 bool invert);

/**
 * \brief Print dec number.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param val[in] Value for printing.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_dec_number_print(const lcd_st7565_cfg* cfg, uint8_t x,
                                 uint8_t y, uint32_t val, bool invert);

/**
 * \brief Print string to center.
 * \param cfg[in] Config of object.
 * \param x_start[in] Start border x pos (0..127).
 * \param x_end[in] Stop border x pos (0..127) (> x_start).
 * \param y[in] Start y pos (0..63).
 * \param str[in] String for printing.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7565_string_to_center_print(const lcd_st7565_cfg* cfg,
                                       uint8_t x_start, uint8_t x_end,
                                       uint8_t y, const char* str, bool invert);
