#pragma once

#include <stdint.h>

static const uint8_t char_font_5x7_stolb[80][5] = {
    {0x3e, 0x51, 0x49, 0x45, 0x3e}, {0x0, 0x42, 0x7f, 0x40, 0x0},
    {0x42, 0x61, 0x51, 0x49, 0x46}, {0x21, 0x41, 0x45, 0x4b, 0x31},
    {0x18, 0x14, 0x12, 0x7f, 0x10}, {0x27, 0x45, 0x45, 0x45, 0x3d},
    {0x3c, 0x4a, 0x49, 0x49, 0x30}, {0x61, 0x11, 0x9, 0x5, 0x3},
    {0x36, 0x49, 0x49, 0x49, 0x36}, {0x6, 0x49, 0x49, 0x29, 0x1e},
    {0x0, 0x36, 0x36, 0x0, 0x0},    {0x0, 0x56, 0x36, 0x0, 0x0},
    {0x8, 0x14, 0x22, 0x41, 0x0},   {0x14, 0x14, 0x14, 0x14, 0x14},
    {0x0, 0x41, 0x22, 0x14, 0x8},   {0x2, 0x1, 0x51, 0x9, 0x6},
    {0x32, 0x49, 0x79, 0x41, 0x3e}, {0x7e, 0x11, 0x11, 0x11, 0x7e},
    {0x7f, 0x49, 0x49, 0x49, 0x36}, {0x3e, 0x41, 0x41, 0x41, 0x22},
    {0x7f, 0x41, 0x41, 0x22, 0x1c}, {0x7f, 0x49, 0x49, 0x49, 0x41},
    {0x7f, 0x9, 0x9, 0x9, 0x1},     {0x3e, 0x41, 0x49, 0x49, 0x7a},
    {0x7f, 0x8, 0x8, 0x8, 0x7f},    {0x0, 0x41, 0x7f, 0x41, 0x0},
    {0x20, 0x40, 0x41, 0x3f, 0x1},  {0x7f, 0x8, 0x14, 0x22, 0x41},
    {0x7f, 0x40, 0x40, 0x40, 0x40}, {0x7f, 0x2, 0xc, 0x2, 0x7f},
    {0x7f, 0x4, 0x8, 0x10, 0x7f},   {0x3e, 0x41, 0x41, 0x41, 0x3e},
    {0x7f, 0x9, 0x9, 0x9, 0x6},     {0x3e, 0x41, 0x51, 0x21, 0x5e},
    {0x7f, 0x9, 0x19, 0x29, 0x46},  {0x46, 0x49, 0x49, 0x49, 0x31},
    {0x3, 0x1, 0x7f, 0x1, 0x3},     {0x3f, 0x40, 0x40, 0x40, 0x3f},
    {0x1f, 0x20, 0x40, 0x20, 0x1f}, {0x3f, 0x40, 0x38, 0x40, 0x3f},
    {0x63, 0x14, 0x8, 0x14, 0x63},  {0x7, 0x8, 0x78, 0x8, 0x7},
    {0x61, 0x51, 0x49, 0x45, 0x43}, {0x0, 0x7f, 0x41, 0x41, 0x0},
    {0x15, 0x16, 0x7c, 0x16, 0x15}, {0x0, 0x41, 0x41, 0x7f, 0x0},
    {0x4, 0x2, 0x1, 0x2, 0x4},      {0x40, 0x40, 0x40, 0x40, 0x40},
    {0x0, 0x1, 0x2, 0x4, 0x0},      {0x20, 0x54, 0x54, 0x54, 0x78},
    {0x7f, 0x48, 0x44, 0x44, 0x38}, {0x38, 0x44, 0x44, 0x44, 0x20},
    {0x38, 0x44, 0x44, 0x48, 0x7f}, {0x38, 0x54, 0x54, 0x54, 0x18},
    {0x8, 0x7e, 0x9, 0x9, 0x2},     {0xc, 0x52, 0x52, 0x52, 0x3e},
    {0x7f, 0x8, 0x4, 0x4, 0x78},    {0x0, 0x44, 0x7d, 0x40, 0x0},
    {0x20, 0x40, 0x44, 0x3d, 0x0},  {0x7f, 0x10, 0x28, 0x44, 0x0},
    {0x0, 0x41, 0x7f, 0x40, 0x0},   {0x7c, 0x4, 0x78, 0x4, 0x78},
    {0x7c, 0x8, 0x4, 0x4, 0x78},    {0x38, 0x44, 0x44, 0x44, 0x38},
    {0x7c, 0x14, 0x14, 0x14, 0x8},  {0x8, 0x14, 0x14, 0x18, 0x7c},
    {0x7c, 0x8, 0x4, 0x4, 0x8},     {0x48, 0x54, 0x54, 0x54, 0x20},
    {0x4, 0x3f, 0x44, 0x44, 0x24},  {0x3c, 0x40, 0x40, 0x20, 0x7c},
    {0x1c, 0x20, 0x40, 0x20, 0x1c}, {0x3c, 0x40, 0x30, 0x40, 0x3c},
    {0x44, 0x28, 0x10, 0x28, 0x44}, {0xc, 0x50, 0x50, 0x50, 0x3c},
    {0x64, 0x54, 0x44, 0x4c, 0x44}, {0x0, 0x0, 0x0, 0x0, 0x0},
    {0x0, 0x0, 0x0, 0x0, 0x0},      {0x0, 0x0, 0x0, 0x0, 0x0},
    {0x0, 0x0, 0x0, 0x0, 0x0},      {0x0, 0x0, 0x0, 0x0, 0x0}};
