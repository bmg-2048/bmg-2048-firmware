#include "lcd_st7920.h"

#include "char_font_7x5_line.h"
#include "number_font_5x3_line.h"

#include <string.h>

#define HEADER_START    0xF8
#define HEADER_RW_WRITE (0 << 2)
#define HEADER_RS_DATA  (1 << 1)
#define HEADER_RS_CMD   (0 << 1)

bool lcd_st7920_is_busy(const lcd_st7920_cfg* cfg, bool* busy) {
    if (!cfg) {
        return false;
    }
    *busy = cfg->ram->busy;
    return true;
}

bool lcd_st7920_init(const lcd_st7920_cfg* cfg) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }

    uint8_t* p = cfg->ram->tx_buffer;

    // Set X/Y for either double-string.
    for (uint8_t i = 0; i < D_STR_NUMBER; i++) {
        // CMD - 0x80 | i.
        *p++ = HEADER_START | HEADER_RW_WRITE | HEADER_RS_CMD;
        *p++ = (0x80 | i) & 0xF0;
        *p++ = (uint8_t)((0x80 | i) << 4);

        // CMD - 0x80.
        *p++ = HEADER_START | HEADER_RW_WRITE | HEADER_RS_CMD;
        *p++ = 0x80;
        *p++ = 0;

        // Add pixels header for either byte with pixels.
        for (uint32_t b = 0; b < (COLUMNS_NUM_IN_D_STR / PIX_NUM_IN_BYTE);
             b++) {
            *p++ = HEADER_START | HEADER_RW_WRITE | HEADER_RS_DATA;
            *p++ = 0;
            *p++ = 0;
        }
    }

    return true;
}

static const uint8_t CMD_OFF         = 0x34;
static const uint8_t CMD_OFF_DATA[3] = {
    HEADER_START | HEADER_RW_WRITE | HEADER_RS_CMD, CMD_OFF & 0xF0,
    (uint8_t)(CMD_OFF << 4)};

static const uint8_t CMD_ON         = 0x36;
static const uint8_t CMD_ON_DATA[3] = {
    HEADER_START | HEADER_RW_WRITE | HEADER_RS_CMD, CMD_ON & 0xF0,
    (uint8_t)(CMD_ON << 4)};

#define STEP_RESET        1
#define STEP_WAIT_SPI_END 3
#define STEP_SEND_CMD_OFF 4
#define STEP_SEND_CMD_ON  5
#define STEP_SEND_BUF     6

bool lcd_st7920_update(const lcd_st7920_cfg* cfg, uint16_t event) {
    if (!cfg) {
        return false;
    }

    if (cfg->ram->step == 0) {
        return true;
    }

    switch (cfg->ram->step) {
        case STEP_RESET:
            DELAY();
            if (!cfg->res(1)) {
                return false;
            }
            cfg->ram->wait = MS_TO_TICK(100);
            cfg->ram->step++;
            break;

        case STEP_RESET + 1:
            DELAY();
            cfg->ram->busy = false;
            cfg->ram->step = 0;
            break;

        case STEP_WAIT_SPI_END:
            if (cfg->tx_port_is_bsy()) {
                return true;
            }
            if (!cfg->cs(0)) {
                return false;
            }
            cfg->ram->busy = false;
            cfg->ram->step = 0;
            break;

        case STEP_SEND_CMD_OFF:
            if (cfg->tx_port_is_bsy()) {
                return true;
            }
            if (!cfg->cs(1)) {
                return false;
            }
            if (!cfg->data_tx_start(CMD_OFF_DATA, 3)) {
                return false;
            }
            cfg->ram->step = STEP_WAIT_SPI_END;
            break;

        case STEP_SEND_CMD_ON:
            if (cfg->tx_port_is_bsy()) {
                return true;
            }
            if (!cfg->cs(1)) {
                return false;
            }
            if (!cfg->data_tx_start(CMD_ON_DATA, 3)) {
                return false;
            }
            cfg->ram->step = STEP_WAIT_SPI_END;
            break;

        case STEP_SEND_BUF:
            if (cfg->tx_port_is_bsy()) {
                return true;
            }
            if (!cfg->cs(1)) {
                return false;
            }
            if (!cfg->data_tx_start(cfg->ram->tx_buffer,
                                    sizeof(cfg->ram->tx_buffer))) {
                return false;
            }
            cfg->ram->step = STEP_WAIT_SPI_END;
            break;

        default:
            break;
    }

    return true;
}

bool lcd_st7920_reset(const lcd_st7920_cfg* cfg) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }
    if (!cfg->res(0)) {
        return false;
    }
    if (!cfg->cs(0)) {
        return false;
    }

    cfg->ram->wait = MS_TO_TICK(100);
    cfg->ram->step = STEP_RESET;
    cfg->ram->busy = true;

    return true;
}

bool lcd_st7920_on(const lcd_st7920_cfg* cfg) {
    if ((!cfg) || (cfg->ram->busy)) {
        return false;
    }
    cfg->ram->step = STEP_SEND_CMD_ON;
    cfg->ram->busy = true;
    return true;
}

bool lcd_st7920_off(const lcd_st7920_cfg* cfg) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }
    cfg->ram->step = STEP_SEND_CMD_OFF;
    cfg->ram->busy = true;
    return true;
}

bool lcd_st7920_send_buffer(const lcd_st7920_cfg* cfg) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }
    cfg->ram->step = STEP_SEND_BUF;
    cfg->ram->busy = true;
    return true;
}

bool lcd_st7920_clear(const lcd_st7920_cfg* cfg) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }

    uint8_t* p = cfg->ram->tx_buffer + D_STR_HEADER_IN_BYTE;

    for (uint8_t i = 0; i < D_STR_NUMBER; i++) {
        for (uint16_t pix_num = 0; pix_num < COLUMNS_NUM_IN_D_STR;
             pix_num          = (uint16_t)(pix_num + PIX_NUM_IN_BYTE)) {
            p++;
            *p++ = 0;
            *p++ = 0;
        }

        p += D_STR_HEADER_IN_BYTE;
    }

    return true;
}

static inline uint8_t* get_str_pointer(const lcd_st7920_cfg* cfg, uint8_t y) {
    if (y < 32) {
        return &cfg->ram->tx_buffer[D_STR_HEADER_IN_BYTE +
                                    D_STR_FULL_SIZE_IN_BYTE * y];
    } else {
        return &cfg->ram
                    ->tx_buffer[D_STR_HEADER_IN_BYTE +
                                D_STR_FULL_SIZE_IN_BYTE * (uint8_t)(y - 32u) +
                                STR_PIX_ONLY_SIZE_IN_BYTE];
    }
}

static inline uint8_t* get_x_pointer(uint8_t* str_pointer, uint8_t x) {
    uint8_t f_byte = x / PIX_NUM_IN_BYTE;

    return str_pointer + f_byte * BYTES_NUM_IN_SPI_PKT + 1 +
           (((x % PIX_NUM_IN_BYTE) >= 4) ? 1 : 0);
}

bool lcd_st7920_horizontal_line_draw(const lcd_st7920_cfg* cfg, uint8_t x,
                                     uint8_t y, uint8_t len, bool invert) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }

    uint8_t* str_p = get_str_pointer(cfg, y);
    uint8_t* x_p   = get_x_pointer(str_p, x);
    bool     l_P   = (((x / 4) % 2) == 0);

    uint8_t x_seek_byte = x % PIX_NUM_IN_HALF_BYTE;
    if (x_seek_byte) {
        uint8_t first_byte_msk = (uint8_t)((0xF0 >> x_seek_byte) & 0xF0);
        if (len <= PIX_NUM_IN_HALF_BYTE - x_seek_byte) {
            first_byte_msk &=
                (uint8_t)(0xF0 << (PIX_NUM_IN_HALF_BYTE - x_seek_byte - len));
            len = 0;
        } else {
            len =
                (uint8_t)(len - ((uint8_t)PIX_NUM_IN_HALF_BYTE - x_seek_byte));
        }

        if (invert) {
            *x_p &= (uint8_t)~first_byte_msk;
        } else {
            *x_p |= first_byte_msk;
        }

        x_p += (l_P) ? 1 : 2;
        l_P = !l_P;
    }

    while (len >= PIX_NUM_IN_HALF_BYTE) {
        if (invert) {
            *x_p = 0;
        } else {
            *x_p = 0xF0;
        }

        x_p += (l_P) ? 1 : 2;
        l_P = !l_P;
        len = (uint8_t)(len - PIX_NUM_IN_HALF_BYTE);
    }

    if (len) {
        uint8_t first_byte_msk =
            (uint8_t)(0xF0u << (uint8_t)(PIX_NUM_IN_HALF_BYTE - len));
        if (invert) {
            *x_p &= (uint8_t)~first_byte_msk;
        } else {
            *x_p |= first_byte_msk;
        }
    }

    return true;
}

bool lcd_st7920_vertical_line_draw(const lcd_st7920_cfg* cfg, uint8_t x,
                                   uint8_t y, uint8_t len, bool invert) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }

    uint8_t msk = (uint8_t)(1u << ((PIX_NUM_IN_HALF_BYTE - 1u) -
                                   (x % PIX_NUM_IN_HALF_BYTE) + 4u));

    do {
        uint8_t* str_p = get_str_pointer(cfg, y);
        uint8_t* x_p   = get_x_pointer(str_p, x);

        if (invert) {
            *x_p &= (uint8_t)~msk;
        } else {
            *x_p |= msk;
        }
        y++;
        len--;
    } while (len);

    return true;
}

bool lcd_st7920_rectangle_draw(const lcd_st7920_cfg* cfg, uint8_t x, uint8_t y,
                               uint8_t width, uint8_t height, bool invert) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }

    if (!lcd_st7920_horizontal_line_draw(cfg, x, y, width,
                                         invert)) {  // - up
        return false;
    }

    if (!lcd_st7920_horizontal_line_draw(
            cfg, x, (uint8_t)(y + height - (uint8_t)1), width,
            invert)) {  // _ down
        return false;
    }

    if (!lcd_st7920_vertical_line_draw(cfg, x, y, height,
                                       invert)) {  // | left
        false;
    }

    return lcd_st7920_vertical_line_draw(cfg, (uint8_t)(x + width - (uint8_t)1),
                                         y, height,
                                         invert);  // | right
}

bool lcd_st7920_rectangle_full_draw(const lcd_st7920_cfg* cfg, uint8_t x,
                                    uint8_t y, uint8_t width, uint8_t height,
                                    bool invert) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }

    do {
        if (!lcd_st7920_horizontal_line_draw(cfg, x, y, width, invert)) {
            return false;
        }
        y++;
        height--;
    } while (height);

    return true;
}

bool lcd_st7920_small_number_print(const lcd_st7920_cfg* cfg, uint8_t x,
                                   uint8_t y, uint8_t val, bool invert) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }

    const uint8_t* img = &number_font_5x3_line[val][0];

    bool    l_P_calc    = (((x / 4) % 2) == 0);
    uint8_t x_seek_byte = x % PIX_NUM_IN_HALF_BYTE;

    for (uint8_t cal_loop = 0; cal_loop < 5; cal_loop++) {
        uint8_t* str_p = get_str_pointer(cfg, y);
        uint8_t* x_p   = get_x_pointer(str_p, x);
        bool     l_P   = l_P_calc;

        uint8_t str = *img++;

        if (invert) {
            *x_p &= ((uint8_t) ~(str >> x_seek_byte)) & 0xF0u;
            x_p += (l_P) ? 1 : 2;
            *x_p &= (uint8_t) ~(str << (PIX_NUM_IN_HALF_BYTE - x_seek_byte));
        } else {
            *x_p |= (uint8_t)(str >> x_seek_byte);
            *x_p &= 0xF0;
            x_p += (l_P) ? 1 : 2;
            *x_p |= (uint8_t)(str << (PIX_NUM_IN_HALF_BYTE - x_seek_byte));
        }

        y++;
    }

    return true;
}

bool lcd_st7920_char_print(const lcd_st7920_cfg* cfg, uint8_t x, uint8_t y,
                           char ch, bool invert) {
    if ((!cfg) || cfg->ram->busy) {
        return false;
    }

    const uint8_t* img = &char_font_7x5_line[(uint8_t)ch - (uint8_t)'0'][0];
    uint8_t        x_seek_byte = x % PIX_NUM_IN_HALF_BYTE;
    bool           l_P_calc    = (((x / 4) % 2) == 0);

    for (uint8_t cal_loop = 0; cal_loop < 7; cal_loop++) {
        uint8_t* str_p = get_str_pointer(cfg, y);
        uint8_t* x_p   = get_x_pointer(str_p, x);

        bool    l_P = l_P_calc;
        uint8_t str = *img++;

        if (invert) {
            *x_p &= ((uint8_t) ~(str >> x_seek_byte)) & 0xF0u;
            x_p += (l_P) ? 1 : 2;
            *x_p &= (uint8_t) ~(str << (PIX_NUM_IN_HALF_BYTE - x_seek_byte));
        } else {
            *x_p |= (uint8_t)(str >> x_seek_byte);
            *x_p &= 0xF0;
            x_p += (l_P) ? 1 : 2;
            *x_p |= (uint8_t)(str << (PIX_NUM_IN_HALF_BYTE - x_seek_byte));
        }

        y++;
    }

    return true;
}
