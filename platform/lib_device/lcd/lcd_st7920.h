#pragma once

#include "app_cfg.h"
#include "event.h"

#define D_STR_NUMBER 32u

#define COLUMNS_NUM_IN_STR   128u
#define COLUMNS_NUM_IN_D_STR (2u * COLUMNS_NUM_IN_STR)
#define PIX_NUM_IN_BYTE      8u
#define PIX_NUM_IN_HALF_BYTE 4u
#define BYTES_NUM_IN_SPI_PKT 3u

#define D_STR_HEADER_IN_BYTE (BYTES_NUM_IN_SPI_PKT * 2u)  // Set X + Set Y.
#define D_STR_PIX_ONLY_SIZE_IN_BYTE \
    ((COLUMNS_NUM_IN_D_STR / PIX_NUM_IN_BYTE) * BYTES_NUM_IN_SPI_PKT)
#define D_STR_FULL_SIZE_IN_BYTE \
    (D_STR_HEADER_IN_BYTE + D_STR_PIX_ONLY_SIZE_IN_BYTE)

#define STR_PIX_ONLY_SIZE_IN_BYTE \
    ((COLUMNS_NUM_IN_STR / PIX_NUM_IN_BYTE) * BYTES_NUM_IN_SPI_PKT)

typedef struct {
    uint8_t tx_buffer[D_STR_FULL_SIZE_IN_BYTE * D_STR_NUMBER];
    bool    busy;
    uint8_t step;
    uint8_t wait;
} lcd_st7920_ram;

typedef bool (*lcd_st7920_pin_set)(bool state);
typedef bool (*lcd_st7920_data_tx_start)(const uint8_t* tx, uint16_t len);
typedef bool (*lcd_st7920_data_tx_port_is_bsy)();

typedef struct {
    lcd_st7920_pin_set             cs;
    lcd_st7920_pin_set             res;
    lcd_st7920_data_tx_start       data_tx_start;
    lcd_st7920_data_tx_port_is_bsy tx_port_is_bsy;
    lcd_st7920_ram*                ram;
} lcd_st7920_cfg;

/**
 * \brief Return busy state of LCD.
 * \param cfg[in] Config of object.
 * \param busy[out] Current busy state of lcd.
 * \return True if cfg is correct.
 */
bool lcd_st7920_is_busy(const lcd_st7920_cfg* cfg, bool* busy);

/**
 * \brief Init internal data in RAM.
 * \param cfg[in] Config of object.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7920_init(const lcd_st7920_cfg* cfg);

/**
 * \brief Update FSM action.
 * \param cfg[in] Config of object.
 * \param event[in] Current event.
 * \return True if cfg is correct and function from lcd_st7920_cfg returns true.
 */
bool lcd_st7920_update(const lcd_st7920_cfg* cfg, uint16_t event);

/**
 * \brief Start reset operation (hardware pin toggle).
 * \param cfg[in] Config of object.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7920_reset(const lcd_st7920_cfg* cfg);

/**
 * \brief Start to send cmd to enable of lcd graphics.
 * \param cfg[in] Config of object.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7920_on(const lcd_st7920_cfg* cfg);

/**
 * \brief Start to send cmd to disable of lcd graphics.
 * \param cfg[in] Config of object.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7920_off(const lcd_st7920_cfg* cfg);

/**
 * \brief Start to send graphics buffer to real LCD.
 * \param cfg[in] Config of object.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7920_send_buffer(const lcd_st7920_cfg* cfg);

/**
 * \brief Clear internal graphics buffer in RAM.
 * \param cfg[in] Config of object.
 * \return True if cfg is correct and object is not busy.
 */
bool lcd_st7920_clear(const lcd_st7920_cfg* cfg);

/**
 * \brief Draw horizontal line.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param len[in] Len of line (1..128). Depending on x.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7920_horizontal_line_draw(const lcd_st7920_cfg* cfg, uint8_t x,
                                     uint8_t y, uint8_t len, bool invert);

/**
 * \brief Draw vertical line.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param len[in] Len of line (1..64). Depending on y.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7920_vertical_line_draw(const lcd_st7920_cfg* cfg, uint8_t x,
                                   uint8_t y, uint8_t len, bool invert);

/**
 * \brief Draw rectangle.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param width[in] Wdth of rectangle (1..128). Depending on x.
 * \param height[in] Height of rectangle (1..64). Depending on y.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7920_rectangle_draw(const lcd_st7920_cfg* cfg, uint8_t x, uint8_t y,
                               uint8_t width, uint8_t height, bool invert);

/**
 * \brief Draw rectangle full.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param width[in] Wdth of rectangle (1..128). Depending on x.
 * \param height[in] Height of rectangle (1..64). Depending on y.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7920_rectangle_full_draw(const lcd_st7920_cfg* cfg, uint8_t x,
                                    uint8_t y, uint8_t width, uint8_t height,
                                    bool invert);
/**
 * \brief Print number.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param val[in] Value (0..F).
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7920_small_number_print(const lcd_st7920_cfg* cfg, uint8_t x,
                                   uint8_t y, uint8_t val, bool invert);

/**
 * \brief Print number.
 * \param cfg[in] Config of object.
 * \param x[in] Start x pos (0..127).
 * \param y[in] Start y pos (0..63).
 * \param ch[in] Char.
 * \param invert[in] true - draw of 0, false - draw of 1.
 * \return True if cfg and parameters is correct and object is not busy.
 */
bool lcd_st7920_char_print(const lcd_st7920_cfg* cfg, uint8_t x, uint8_t y,
                           char ch, bool invert);
