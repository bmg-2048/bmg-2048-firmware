#include "action_joystick.h"

#include "event.h"

#define ADC_MIDDLE_VALUE ((uint16_t)(4096 / 2))
#define BORDER_VALUE     ((uint16_t)((((float)ADC_MIDDLE_VALUE) * 0.6f)))

uint8_t is_deviated(int32_t x_value, int32_t y_value) {
    if (x_value > BORDER_VALUE) {
        return B_RIGHT;
    } else if (x_value < -BORDER_VALUE) {
        return B_LEFT;
    } else if (y_value > BORDER_VALUE) {
        return B_DOWN;
    } else if (y_value < -BORDER_VALUE) {
        return B_UP;
    }
    return B_NO;
}

bool action_joystick_update(const action_joystick_cfg* cfg, uint16_t event) {
    if (!cfg) {
        return false;
    }

    int32_t x;
    int32_t y;
    uint8_t button_num = B_NO;

    switch (cfg->ram->step) {
        case 0:  // Check center pos.
            x          = ((int32_t)cfg->get_x()) - ADC_MIDDLE_VALUE;
            y          = ((int32_t)cfg->get_y()) - ADC_MIDDLE_VALUE;
            button_num = is_deviated(x, y);
            if (button_num != B_NO) {
                cfg->ram->down_counter = cfg->wait_tick_num;
                break;
            }
            cfg->ram->step++;
            break;

        case 1:
            if (event != EVENT_SYSTIC) {
                return true;
            }
            cfg->ram->down_counter--;
            if (cfg->ram->down_counter != 0) {
                cfg->ram->step = 0;
                break;
            }
            cfg->ram->captured_state = B_NO;
            cfg->ram->step++;
            break;

        case 2:  // Wait not center.
            x          = ((int32_t)cfg->get_x()) - ADC_MIDDLE_VALUE;
            y          = ((int32_t)cfg->get_y()) - ADC_MIDDLE_VALUE;
            button_num = is_deviated(x, y);
            if (button_num == B_NO) {
                cfg->ram->down_counter = cfg->wait_tick_num;
                break;
            }
            cfg->ram->captured_state = button_num;
            cfg->ram->step++;
            break;

        case 3:
            if (event != EVENT_SYSTIC) {
                return true;
            }
            x          = ((int32_t)cfg->get_x()) - ADC_MIDDLE_VALUE;
            y          = ((int32_t)cfg->get_y()) - ADC_MIDDLE_VALUE;
            button_num = is_deviated(x, y);
            if (button_num != cfg->ram->captured_state) {
                cfg->ram->down_counter = cfg->wait_tick_num;
                cfg->ram->step         = 2;
                break;
            }
            cfg->ram->down_counter--;
            if (cfg->ram->down_counter != 0) {
                break;
            }

            cfg->ram->final_state = cfg->ram->captured_state;

            cfg->ram->step = 0;
            break;
    }

    return true;
}

bool action_joystick_buttons_state_get(const action_joystick_cfg* cfg,
                                       bool*                      clicked) {
    if (!cfg) {
        return false;
    }

    *clicked = cfg->ram->final_state != B_NO;

    return true;
}

bool action_joystick_buttons_num_get(const action_joystick_cfg* cfg,
                                     uint8_t*                   button_num) {
    if (!cfg) {
        return false;
    }

    if (cfg->ram->final_state == B_NO) {
        return false;
    }

    *button_num           = cfg->ram->final_state;
    cfg->ram->final_state = B_NO;

    return true;
}
