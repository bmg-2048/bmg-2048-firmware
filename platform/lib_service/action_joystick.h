#pragma once

#include "app_cfg.h"

#include <stdbool.h>
#include <stdint.h>

typedef uint16_t (*action_joystick_get_adc_x)();
typedef uint16_t (*action_joystick_get_adc_y)();

#define ACTION_JOYSTICK_QUEUE_LEN 3

typedef struct {
    uint8_t final_state;
    uint8_t captured_state;
    uint8_t down_counter;
    uint8_t step;
} action_joystick_ram;

typedef struct {
    action_joystick_get_adc_x get_x;
    action_joystick_get_adc_y get_y;
    uint8_t                   wait_tick_num;
    action_joystick_ram*      ram;
} action_joystick_cfg;

bool action_joystick_update(const action_joystick_cfg* cfg, uint16_t event);

bool action_joystick_buttons_state_get(const action_joystick_cfg* cfg,
                                       bool*                      clicked);

bool action_joystick_buttons_num_get(const action_joystick_cfg* cfg,
                                     uint8_t*                   button_num);
