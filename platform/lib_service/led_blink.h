#pragma once

#include "event.h"

typedef struct {
    uint16_t time_counter;
    bool     led_state;
} led_blink_ram;

typedef bool (*led_set)(bool state);

typedef struct {
    uint16_t       period;  // In ticks.
    led_set        led;
    led_blink_ram* ram;
} led_blink_cfg;

bool led_blink_update(const led_blink_cfg* cfg, uint16_t event);
