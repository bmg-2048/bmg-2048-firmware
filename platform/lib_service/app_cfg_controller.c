#include "app_cfg_controller.h"

static bool crc32_get(const app_cfg_controller_cfg* const cfg,
                      uint8_t eeprom_call_start, uint16_t word_len,
                      uint32_t* ret_crc) {
    cfg->crc_reset();
    do {
        uint32_t item_value = 0;
        if (!cfg->eeprom_word_read(eeprom_call_start, &item_value)) {
            return false;
        }
        cfg->crc_item_add(item_value);
        word_len--;
        eeprom_call_start++;
    } while (word_len);

    *ret_crc = cfg->crc_result_get();
    return true;
}

bool app_cfg_controller_update(const app_cfg_controller_cfg* const cfg) {
    if (!cfg) {
        return false;
    }

    if (!cfg->ram->step) {
        return true;
    }

    uint32_t crc  = 0;
    bool     busy = false;

    switch (cfg->ram->step) {
        case 1:
            if (!cfg->eeprom_is_busy(&busy)) {
                return false;
            }
            if (busy) {
                break;
            }
            cfg->ram->busy = false;
            cfg->ram->step = 0;
            break;

        case 2:
            if (!cfg->eeprom_is_busy(&busy)) {
                return false;
            }
            if (busy) {
                break;
            }
            if (!cfg->eeprom_array_start_to_write(
                    (uint8_t)(cfg->ram->eeprom_word_num_start + 1u),
                    cfg->ram->str, cfg->ram->word_len)) {
                return false;
            }
            cfg->ram->step++;
            break;

        case 3:
            if (!cfg->eeprom_is_busy(&busy)) {
                return false;
            }
            if (busy) {
                break;
            }
            if (!crc32_get(cfg, (uint8_t)(cfg->ram->eeprom_word_num_start + 1u),
                           cfg->ram->word_len, &crc)) {
                return false;
            }
            if (!cfg->eeprom_word_start_to_write(
                    cfg->ram->eeprom_word_num_start, crc)) {
                return false;
            }
            cfg->ram->step = 1;
            break;
    }

    return true;
}

bool app_cfg_controller_is_busy(const app_cfg_controller_cfg* const cfg,
                                bool*                               busy) {
    if (!cfg) {
        return false;
    }

    *busy = cfg->ram->busy;

    return true;
}

bool app_cfg_controller_cfg_start_to_save(
    const app_cfg_controller_cfg* const cfg, uint32_t* str, uint8_t word_len,
    uint8_t eeprom_word_num_start) {
    if (!cfg) {
        return false;
    }
    if (cfg->ram->busy) {
        return false;
    }
    if (!str) {
        return false;
    }

    cfg->ram->eeprom_word_num_start = eeprom_word_num_start;
    cfg->ram->str                   = str;
    cfg->ram->word_len              = word_len;
    cfg->ram->step                  = 2;
    cfg->ram->busy                  = true;
    return true;
}

bool app_cfg_controller_cfg_read(const app_cfg_controller_cfg* const cfg,
                                 uint32_t* str, uint8_t word_len,
                                 uint8_t eeprom_word_num_start) {
    if (!cfg) {
        return false;
    }
    if (cfg->ram->busy) {
        return false;
    }
    if (!str) {
        return false;
    }
    return cfg->eeprom_array_read((uint8_t)(eeprom_word_num_start + 1u),
                                  word_len, str);
}

bool app_cfg_controller_is_cfg_valid(const app_cfg_controller_cfg* const cfg,
                                     uint8_t  eeprom_word_num_start,
                                     uint16_t word_len, bool* valid) {
    uint32_t eeprom_crc = 0u;
    if (!cfg->eeprom_word_read(eeprom_word_num_start, &eeprom_crc)) {
        return false;
    }
    uint32_t module_crc = 0u;
    if (!crc32_get(cfg, (uint8_t)(eeprom_word_num_start + 1u), word_len,
                   &module_crc)) {
        return false;
    }
    *valid = eeprom_crc == module_crc;
    return true;
}
