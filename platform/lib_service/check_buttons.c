#include "check_buttons.h"

bool check_buttons_update(const check_buttons_cfg* cfg, uint16_t event) {
    if (!cfg) {
        return false;
    }

    if (event != EVENT_SYSTIC) {
        return true;
    }

    for (uint8_t ch = 0; ch < cfg->button_number; ch++) {
        bool state = false;
        if (!cfg->pin_state_get(ch, &state)) {
            return false;
        }
        if (state) {
            if (cfg->buttons_ram[ch].counter == cfg->click_time) {
                cfg->buttons_ram[ch].counter++;
                cfg->buttons_ram[ch].pushed  = true;
                cfg->buttons_ram[ch].clicked = true;
            } else {
                cfg->buttons_ram[ch].counter++;
            }
        } else {
            cfg->buttons_ram[ch].counter = 0;
            cfg->buttons_ram[ch].pushed  = false;
        }
    }

    return true;
}

bool check_buttons_state_get(const check_buttons_cfg* cfg, bool* clicked) {
    if (!cfg || !clicked) {
        return false;
    }

    for (uint8_t ch = 0; ch < cfg->button_number; ch++) {
        if (cfg->buttons_ram[ch].clicked) {
            *clicked = true;
            return true;
        }
    }

    *clicked = false;
    return true;
}

bool check_buttons_num_get(const check_buttons_cfg* cfg, uint8_t* button_num) {
    if (!cfg || !button_num) {
        return false;
    }

    for (uint8_t ch = 0; ch < cfg->button_number; ch++) {
        if (cfg->buttons_ram[ch].clicked) {
            cfg->buttons_ram[ch].clicked = false;
            *button_num                  = ch;
            return true;
        }
    }

    return false;
}

bool check_buttonspush_state_get(const check_buttons_cfg* cfg,
                                 uint8_t button_num, bool* pushed) {
    if (!cfg) {
        return false;
    }

    if (button_num >= cfg->button_number) {
        return false;
    }

    *pushed = cfg->buttons_ram[button_num].pushed;
    return true;
}
