#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef struct {
    uint8_t   step;
    bool      busy;
    uint32_t* str;
    uint8_t   word_len;
    uint8_t   eeprom_word_num_start;
} app_cfg_controller_ram;

typedef bool (*app_cfg_controller_eeprom_is_busy)(bool* busy);
typedef bool (*app_cfg_controller_eeprom_array_start_to_write)(
    uint8_t eeprom_word_num, uint32_t* data, uint8_t data_word_number);
typedef bool (*app_cfg_controller_eeprom_word_read)(uint8_t   eeprom_word_num,
                                                    uint32_t* ret_data);
typedef bool (*app_cfg_controller_eeprom_word_start_to_write)(
    uint8_t eeprom_word_num, uint32_t data);
typedef bool (*app_cfg_controller_eeprom_array_read)(uint8_t   eeprom_word_num,
                                                     uint8_t   data_word_number,
                                                     uint32_t* ret_buf);

typedef void (*app_cfg_controller_crc_reset)();
typedef void (*app_cfg_controller_crc_item_add)(uint32_t item);
typedef uint32_t (*app_cfg_controller_crc_result_get)();

typedef struct {
    app_cfg_controller_eeprom_is_busy              eeprom_is_busy;
    app_cfg_controller_eeprom_array_start_to_write eeprom_array_start_to_write;
    app_cfg_controller_eeprom_word_read            eeprom_word_read;
    app_cfg_controller_eeprom_word_start_to_write  eeprom_word_start_to_write;
    app_cfg_controller_eeprom_array_read           eeprom_array_read;
    app_cfg_controller_crc_reset                   crc_reset;
    app_cfg_controller_crc_item_add                crc_item_add;
    app_cfg_controller_crc_result_get              crc_result_get;
    app_cfg_controller_ram*                        ram;
} app_cfg_controller_cfg;

/**
 *
 */
bool app_cfg_controller_update(const app_cfg_controller_cfg* cfg);

/**
 *
 */
bool app_cfg_controller_is_busy(const app_cfg_controller_cfg* cfg, bool* busy);

/**
 *
 */
bool app_cfg_controller_cfg_start_to_save(const app_cfg_controller_cfg* cfg,
                                          uint32_t* str, uint8_t word_len,
                                          uint8_t eeprom_word_num_start);

/**
 *
 */
bool app_cfg_controller_cfg_read(const app_cfg_controller_cfg* cfg,
                                 uint32_t* str, uint8_t word_len,
                                 uint8_t eeprom_word_num_start);

/**
 *
 */
bool app_cfg_controller_is_cfg_valid(const app_cfg_controller_cfg* cfg,
                                     uint8_t  eeprom_word_num_start,
                                     uint16_t word_len, bool* valid);
