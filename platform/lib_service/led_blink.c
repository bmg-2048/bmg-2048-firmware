#include "led_blink.h"

bool led_blink_update(const led_blink_cfg* cfg, uint16_t event) {
    if (!cfg) {
        return false;
    }

    if (event != EVENT_SYSTIC) {
        return true;
    }

    cfg->ram->time_counter++;

    if (cfg->ram->time_counter != cfg->period) {
        return true;
    }

    cfg->ram->time_counter = 0;
    cfg->ram->led_state    = !cfg->ram->led_state;

    return cfg->led(cfg->ram->led_state);
}
