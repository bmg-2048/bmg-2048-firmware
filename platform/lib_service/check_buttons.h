#pragma once

#include "event.h"

typedef bool (*check_buttons_pin_state_get)(uint8_t pin_number,
                                            bool*   ret_state);

typedef struct {
    uint8_t counter;
    bool    pushed;
    bool    clicked;
} check_buttons_ram;

typedef struct {
    check_buttons_pin_state_get pin_state_get;
    uint8_t                     button_number;
    check_buttons_ram*          buttons_ram;  // Array. Size button_number.
    uint8_t                     click_time;   // In tick.
} check_buttons_cfg;

bool check_buttons_update(const check_buttons_cfg* cfg, uint16_t event);

/**
 * \brief Retrun state of button clicked.
 * \param cfg[in] Config of object.
 * \param clicked[out] true if any kay was clicked.
 * \return True if cfg is correct and clicked is not null.
 */
bool check_buttons_state_get(const check_buttons_cfg* cfg, bool* clicked);

/**
 * \brief Retrun number of clicked button.
 * \param cfg[in] Config of object.
 * \param button_num[out] number of cliked button.
 * \return True if cfg is correct and clicked is not null and button was cliked.
 */
bool check_buttons_num_get(const check_buttons_cfg* cfg, uint8_t* button_num);

/**
 * \brief Retrun number of clicked button.
 * \param cfg[in] Config of object.
 * \param button_num[in] number of button.
 * \param pushed[щге] state of button.
 * \return True if cfg is correct and clicked is not null and button was cliked.
 */
bool check_buttonspush_state_get(const check_buttons_cfg* cfg,
                                 uint8_t button_num, bool* pushed);
