#include "lib_app_controller.h"
#include "base_funcs.h"
#include "stddef.h"

#define STEP_LCD_RESET   0
#define STEP_LCD_INIT    1
#define STEP_LCD_OFF     2
#define STEP_LCD_CLEAR   3
#define STEP_LCD_ON      4
#define STEP_GAME_INIT   5
#define STEP_GAME_UPDATE 6

static void step_lcd_reset(const app_controller_cfg* cfg, uint16_t event) {
    (void)event;
    bool buf_bool = false;
    if (!cfg->f->lcd_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    if (!cfg->f->lcd_reset()) {
        return;
    }
    cfg->ram->step = STEP_LCD_INIT;
}

static void step_lcd_init(const app_controller_cfg* cfg, uint16_t event) {
    (void)event;
    bool buf_bool = false;
    if (!cfg->f->lcd_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    if (!cfg->f->lcd_init()) {
        return;
    }
    cfg->ram->step = STEP_LCD_OFF;
}

static void step_lcd_off(const app_controller_cfg* cfg, uint16_t event) {
    (void)event;
    bool buf_bool = false;
    if (!cfg->f->lcd_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    if (!cfg->f->lcd_off()) {
        return;
    }
    cfg->ram->step = STEP_LCD_CLEAR;
}

static void step_lcd_clear(const app_controller_cfg* cfg, uint16_t event) {
    (void)event;
    bool buf_bool = false;
    if (!cfg->f->lcd_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    if (!cfg->f->lcd_send_buffer()) {
        return;
    }
    cfg->ram->step = STEP_LCD_ON;
}

static void step_lcd_on(const app_controller_cfg* cfg, uint16_t event) {
    (void)event;
    bool buf_bool = false;
    if (!cfg->f->lcd_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    if (!cfg->f->lcd_on()) {
        return;
    }
    cfg->ram->cur_game = 0;
    cfg->ram->step     = STEP_GAME_INIT;
}

static void step_game_init(const app_controller_cfg* cfg, uint16_t event) {
    (void)event;
    bool buf_bool = false;
    if (!cfg->f->lcd_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    cfg->games[cfg->ram->cur_game].init();
    cfg->ram->step = STEP_GAME_UPDATE;
}

static void step_game_update(const app_controller_cfg* cfg, uint16_t event) {
    cfg->games[cfg->ram->cur_game].update(event);
}

typedef void (*step_f)(const app_controller_cfg* cfg, uint16_t event);

static const step_f steps[7] = {
    step_lcd_reset,   // STEP_LCD_RESET
    step_lcd_init,    // STEP_LCD_INIT
    step_lcd_off,     // STEP_LCD_OFF
    step_lcd_clear,   // STEP_LCD_CLEAR
    step_lcd_on,      // STEP_LCD_ON
    step_game_init,   // STEP_GAME_INIT
    step_game_update  // STEP_GAME_UPDATE
};

void app_controller_update(const app_controller_cfg* cfg, uint16_t event) {
    return steps[cfg->ram->step](cfg, event);
}
