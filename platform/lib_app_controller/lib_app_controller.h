#pragma once

#include "app_ref_table.h"

typedef struct {
    uint8_t cur_game;
    uint8_t step;
} app_controller_ram;

typedef void (*game_init)();
typedef void (*game_update)(uint16_t event);

typedef struct {
    game_init   init;
    game_update update;
} app_controller_game_item;

typedef struct {
    const app_func_ref_table*       f;
    const app_controller_game_item* games;
    uint8_t                         game_number;
    app_controller_ram*             ram;
} app_controller_cfg;

void app_controller_update(const app_controller_cfg* cfg, uint16_t event);
