#include "app_cfg_controller.h"
#include "mc_level.h"

static app_cfg_controller_ram ram = {0};

static const app_cfg_controller_cfg cfg = {
    .eeprom_is_busy              = &mc_eeprom_is_busy,
    .eeprom_array_start_to_write = &mc_eeprom_array_start_to_write,
    .eeprom_word_read            = &mc_eeprom_word_read,
    .eeprom_word_start_to_write  = &mc_eeprom_word_start_to_write,
    .eeprom_array_read           = &mc_eeprom_array_read,
    .crc_reset                   = &mc_crc_reset,
    .crc_item_add                = &mc_crc_item_add,
    .crc_result_get              = &mc_crc_result_get,
    .ram                         = &ram};

bool service_app_cfg_controller_update() {
    return app_cfg_controller_update(&cfg);
}

bool service_app_cfg_controller_is_busy(bool* busy) {
    return app_cfg_controller_is_busy(&cfg, busy);
}

bool service_app_cfg_controller_cfg_start_to_save(
    uint32_t* str, uint8_t word_len, uint8_t eeprom_word_num_start) {
    return app_cfg_controller_cfg_start_to_save(&cfg, str, word_len,
                                                eeprom_word_num_start);
}

bool service_app_cfg_controller_cfg_read(uint32_t* str, uint8_t word_len,
                                         uint8_t eeprom_word_num_start) {
    return app_cfg_controller_cfg_read(&cfg, str, word_len,
                                       eeprom_word_num_start);
}

bool service_app_cfg_controller_is_cfg_valid(uint8_t  eeprom_word_num_start,
                                             uint16_t word_len, bool* valid) {
    return app_cfg_controller_is_cfg_valid(&cfg, eeprom_word_num_start,
                                           word_len, valid);
}
