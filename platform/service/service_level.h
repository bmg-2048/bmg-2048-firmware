#pragma once

#include <stdbool.h>
#include <stdint.h>

/**
 *
 */
bool service_led_blink_update(uint16_t event);

/**
 *
 */
bool service_app_cfg_controller_is_busy(bool* busy);

/**
 *
 */
bool service_app_cfg_controller_update();

/**
 *
 */
bool service_app_cfg_controller_cfg_start_to_save(
    uint32_t* str, uint8_t word_len, uint32_t eeprom_word_num_start);

/**
 *
 */
bool service_app_cfg_controller_cfg_read(uint32_t* str, uint8_t word_len,
                                         uint32_t eeprom_word_num_start);

/**
 *
 */
bool service_app_cfg_controller_is_cfg_valid(uint32_t eeprom_word_num_start,
                                             uint16_t word_len, bool* valid);
