#include "led_blink.h"
#include "mc_level.h"

static led_blink_ram l_blink_ram = {0};

static const led_blink_cfg l_blink_cfg = {.period = 1000,
                                          .led    = &mc_led_pin_set,
                                          .ram    = &l_blink_ram};

bool service_led_blink_update(uint16_t event) {
    return led_blink_update(&l_blink_cfg, event);
}
