#include "app_ref_table.h"
#include "mc_level.h"
#include "pcb_level.h"
#include "service_level.h"

const app_func_ref_table app_table = {
    // ADC.
    .adc_x_get = &mc_adc_x_get,
    .adc_y_get = &mc_adc_y_get,

    // SYSTICK.
    .systick_counter_get = &mc_systick_counter_get,

    // LCD.
    .lcd_reset                  = &pcb_lcd_reset,
    .lcd_init                   = &pcb_lcd_init,
    .lcd_on                     = &pcb_lcd_on,
    .lcd_off                    = &pcb_lcd_off,
    .lcd_is_busy                = &pcb_lcd_is_busy,
    .lcd_send_buffer            = &pcb_lcd_send_buffer,
    .lcd_clear                  = &pcb_lcd_clear,
    .lcd_horizontal_line_draw   = &pcb_lcd_horizontal_line_draw,
    .lcd_vertical_line_draw     = &pcb_lcd_vertical_line_draw,
    .lcd_rectangle_draw         = &pcb_lcd_rectangle_draw,
    .lcd_rectangle_full_draw    = &pcb_lcd_rectangle_full_draw,
    .lcd_small_one_number_print = &pcb_lcd_small_one_number_print,
    .lcd_char_print             = &pcb_lcd_char_print,
    .lcd_string_print           = &pcb_lcd_string_print,
    .lcd_dec_small_number_print = &pcb_lcd_dec_small_number_print,
    .lcd_dec_small_number_to_center_print =
        &pcb_lcd_dec_small_number_to_center_print,
    .lcd_hex_small_number_print = &pcb_lcd_hex_small_number_print,
    .lcd_dec_number_print       = &pcb_lcd_dec_number_print,
    .lcd_hex_number_print       = &pcb_lcd_hex_number_print,
    .lcd_string_to_center_print = &pcb_lcd_string_to_center_print,

    // BUTTONS.
    .check_buttons_state_get      = &pcb_check_buttons_state_get,
    .check_buttons_num_get        = &pcb_check_buttons_num_get,
    .check_buttons_push_state_get = &pcb_check_buttons_push_state_get,

    // JOY.
    .action_joystick_state_get = &pcb_action_joystick_state_get,
    .action_joystick_num_get   = &pcb_action_joystick_num_get,

    // CFG.
    .cfg_controller_is_busy = &service_app_cfg_controller_is_busy,
    .cfg_controller_cfg_start_to_save =
        &service_app_cfg_controller_cfg_start_to_save,
    .cfg_controller_cfg_read     = &service_app_cfg_controller_cfg_read,
    .cfg_controller_is_cfg_valid = &service_app_cfg_controller_is_cfg_valid};
