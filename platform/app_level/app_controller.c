#include "app_level.h"
#include "app_ref_table.h"
#include "lib_app_controller.h"

#define GAME_NUM 2

static const app_controller_game_item games[GAME_NUM] = {
    {
        .init   = &app_board_test_init,
        .update = &app_board_test_update,
    },
    {
        .init   = &app_game_2048_init,
        .update = &app_game_2048_update,
    }};

static app_controller_ram ram = {0};

app_controller_cfg cfg = {.f           = &app_table,
                          .games       = games,
                          .game_number = GAME_NUM,
                          .ram         = &ram};


void app_app_controller_update(uint16_t event) {
    app_controller_update(&cfg, event);
}
