#pragma once

#include <stdbool.h>
#include <stdint.h>

void app_app_controller_update(uint16_t event);

void app_board_test_init();
void app_board_test_update(uint16_t event);

void app_game_2048_init();
void app_game_2048_update(uint16_t event);
