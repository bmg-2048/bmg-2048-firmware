#include "game_2048.h"
#include "app_ref_table.h"

static game_2048_eeprom eeprom = {0};
static game_2048_ram    ram    = {0};

static const game_2048_cfg cfg = {.f      = &app_table,
                                  .ram    = &ram,
                                  .eeprom = &eeprom};

void app_game_2048_init() {
    game_2048_init(&cfg);
}

void app_game_2048_update(uint16_t event) {
    game_2048_update(&cfg, event);
}
