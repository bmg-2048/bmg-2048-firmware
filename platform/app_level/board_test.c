#include "board_test.h"
#include "app_ref_table.h"

static board_test_ram joy_test_ram = {0};

static const board_test_cfg joy_test_cfg = {
    .f   = &app_table,
    .ram = &joy_test_ram,
};

void app_board_test_init() {
    board_test_init(&joy_test_cfg);
}

void app_board_test_update(uint16_t event) {
    board_test_update(&joy_test_cfg, event);
}