#pragma once

#include <stdbool.h>
#include <stdint.h>

// ADC.
typedef uint16_t (*app_ref_adc_x_get)();
typedef uint16_t (*app_ref_adc_y_get)();

// SYSTIC.
typedef uint64_t (*app_ref_systick_counter_get)();

// LCD.
typedef bool (*app_ref_lcd_reset)();
typedef bool (*app_ref_lcd_init)();
typedef bool (*app_ref_lcd_on)();
typedef bool (*app_ref_lcd_off)();
typedef bool (*app_ref_lcd_is_busy)(bool* busy);
typedef bool (*app_ref_lcd_send_buffer)();
typedef bool (*app_ref_lcd_clear)();
typedef bool (*app_ref_lcd_horizontal_line_draw)(uint8_t x, uint8_t y,
                                                 uint8_t len, bool invert);
typedef bool (*app_ref_lcd_vertical_line_draw)(uint8_t x, uint8_t y,
                                               uint8_t len, bool invert);
typedef bool (*app_ref_lcd_rectangle_draw)(uint8_t x, uint8_t y, uint8_t width,
                                           uint8_t height, bool invert);
typedef bool (*app_ref_lcd_rectangle_full_draw)(uint8_t x, uint8_t y,
                                                uint8_t width, uint8_t height,
                                                bool invert);
typedef bool (*app_ref_lcd_small_one_number_print)(uint8_t x, uint8_t y,
                                                   uint8_t val, bool invert);
typedef bool (*app_ref_lcd_char_print)(uint8_t x, uint8_t y, char ch,
                                       bool invert);
typedef bool (*app_ref_lcd_string_print)(uint8_t x, uint8_t y, const char* str,
                                         bool invert);
typedef bool (*app_ref_lcd_dec_small_number_print)(uint8_t x, uint8_t y,
                                                   uint32_t val, bool invert);
typedef bool (*app_ref_lcd_dec_small_number_to_center_print)(
    uint8_t x_start, uint8_t x_end, uint8_t y, uint32_t val, bool invert);
typedef bool (*app_ref_lcd_hex_small_number_print)(uint8_t x, uint8_t y,
                                                   uint32_t val, uint8_t num_ch,
                                                   bool invert);
typedef bool (*app_ref_lcd_dec_number_print)(uint8_t x, uint8_t y, uint32_t val,
                                             bool invert);
typedef bool (*app_ref_lcd_hex_number_print)(uint8_t x, uint8_t y, uint32_t val,
                                             uint8_t num_ch, bool invert);
typedef bool (*app_ref_lcd_string_to_center_print)(uint8_t x_start,
                                                   uint8_t x_end, uint8_t y,
                                                   const char* str,
                                                   bool        invert);

// BUTTONS.
typedef bool (*app_ref_check_buttons_state_get)(bool* clicked);
typedef bool (*app_ref_check_buttons_num_get)(uint8_t* button_num);
typedef bool (*app_ref_check_buttons_push_state_get)(uint8_t button_num,
                                                     bool*   pushed);

// JOY.
typedef bool (*app_ref_action_joystick_state_get)(bool* clicked);
typedef bool (*app_ref_action_joystick_num_get)(uint8_t* button_num);

// CFG.
typedef bool (*app_ref_cfg_controller_is_busy)(bool* busy);
typedef bool (*app_ref_cfg_controller_cfg_start_to_save)(
    uint32_t* str, uint8_t word_len, uint32_t eeprom_word_num_start);
typedef bool (*app_ref_cfg_controller_cfg_read)(uint32_t* str, uint8_t word_len,
                                                uint32_t eeprom_word_num_start);
typedef bool (*app_ref_cfg_controller_is_cfg_valid)(
    uint32_t eeprom_word_num_start, uint16_t word_len, bool* valid);

typedef struct {
    // ADC.
    app_ref_adc_x_get adc_x_get;
    app_ref_adc_y_get adc_y_get;

    // SYSTICK.
    app_ref_systick_counter_get systick_counter_get;

    // LCD.
    app_ref_lcd_reset                  lcd_reset;
    app_ref_lcd_init                   lcd_init;
    app_ref_lcd_on                     lcd_on;
    app_ref_lcd_off                    lcd_off;
    app_ref_lcd_is_busy                lcd_is_busy;
    app_ref_lcd_send_buffer            lcd_send_buffer;
    app_ref_lcd_clear                  lcd_clear;
    app_ref_lcd_horizontal_line_draw   lcd_horizontal_line_draw;
    app_ref_lcd_vertical_line_draw     lcd_vertical_line_draw;
    app_ref_lcd_rectangle_draw         lcd_rectangle_draw;
    app_ref_lcd_rectangle_full_draw    lcd_rectangle_full_draw;
    app_ref_lcd_small_one_number_print lcd_small_one_number_print;
    app_ref_lcd_char_print             lcd_char_print;
    app_ref_lcd_string_print           lcd_string_print;
    app_ref_lcd_dec_small_number_print lcd_dec_small_number_print;
    app_ref_lcd_dec_small_number_to_center_print
                                       lcd_dec_small_number_to_center_print;
    app_ref_lcd_hex_small_number_print lcd_hex_small_number_print;
    app_ref_lcd_dec_number_print       lcd_dec_number_print;
    app_ref_lcd_hex_number_print       lcd_hex_number_print;
    app_ref_lcd_string_to_center_print lcd_string_to_center_print;

    // BUTTONS.
    app_ref_check_buttons_state_get      check_buttons_state_get;
    app_ref_check_buttons_num_get        check_buttons_num_get;
    app_ref_check_buttons_push_state_get check_buttons_push_state_get;

    // JOY.
    app_ref_action_joystick_state_get action_joystick_state_get;
    app_ref_action_joystick_num_get   action_joystick_num_get;

    // CFG.
    app_ref_cfg_controller_is_busy           cfg_controller_is_busy;
    app_ref_cfg_controller_cfg_start_to_save cfg_controller_cfg_start_to_save;
    app_ref_cfg_controller_cfg_read          cfg_controller_cfg_read;
    app_ref_cfg_controller_is_cfg_valid      cfg_controller_is_cfg_valid;
} app_func_ref_table;

extern const app_func_ref_table app_table;
