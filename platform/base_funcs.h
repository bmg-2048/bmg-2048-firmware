#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * \brief Check and exit.
 */
#define CAE(x) \
    if (x)     \
    return false

/**
 * \brief Attribute always inline.
 */
#define AAI __attribute__((always_inline)) inline

/**
 * \brief Attribute always inline static.
 */
#define AAIS AAI static

/**
 * \brief Attribute noreturn function.
 */
#define ANR __attribute__((noreturn))

/**
 * \brief Attribute section.
 */
#define AS(s) __attribute__((section(s)))
