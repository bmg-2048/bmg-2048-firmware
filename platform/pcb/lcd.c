#include "lcd_st7565.h"
#include "mc_level.h"

static lcd_st7565_ram lcd_ram = {0};

static const lcd_st7565_cfg lcd_cfg = {.cs             = &mc_lcd_cs_pin_set,
                                       .a0             = &mc_lcd_a0_pin_set,
                                       .data_tx_start  = &mc_spi_tx_start,
                                       .tx_port_is_bsy = &mc_spi_is_busy,
                                       .ram            = &lcd_ram};

bool pcb_lcd_is_busy(bool* busy) {
    return lcd_st7565_is_busy(&lcd_cfg, busy);
}

bool pcb_lcd_init() {
    return lcd_st7565_init(&lcd_cfg);
}

bool pcb_lcd_update(uint16_t event) {
    return lcd_st7565_update(&lcd_cfg, event);
}

bool pcb_lcd_reset() {
    return lcd_st7565_reset(&lcd_cfg);
}

bool pcb_lcd_on() {
    return lcd_st7565_on(&lcd_cfg);
}

bool pcb_lcd_off() {
    return lcd_st7565_off(&lcd_cfg);
}

bool pcb_lcd_send_buffer() {
    return lcd_st7565_send_buffer(&lcd_cfg);
}

bool pcb_lcd_clear() {
    return lcd_st7565_clear(&lcd_cfg);
}

bool pcb_lcd_horizontal_line_draw(uint8_t x, uint8_t y, uint8_t len,
                                  bool invert) {
    return lcd_st7565_horizontal_line_draw(&lcd_cfg, x, y, len, invert);
}

bool pcb_lcd_vertical_line_draw(uint8_t x, uint8_t y, uint8_t len,
                                bool invert) {
    return lcd_st7565_vertical_line_draw(&lcd_cfg, x, y, len, invert);
}

bool pcb_lcd_rectangle_draw(uint8_t x, uint8_t y, uint8_t width, uint8_t height,
                            bool invert) {
    return lcd_st7565_rectangle_draw(&lcd_cfg, x, y, width, height, invert);
}

bool pcb_lcd_rectangle_full_draw(uint8_t x, uint8_t y, uint8_t width,
                                 uint8_t height, bool invert) {
    return lcd_st7565_rectangle_full_draw(&lcd_cfg, x, y, width, height,
                                          invert);
}

bool pcb_lcd_small_one_number_print(uint8_t x, uint8_t y, uint8_t val,
                                    bool invert) {
    return lcd_st7565_small_one_number_print(&lcd_cfg, x, y, val, invert);
}

bool pcb_lcd_char_print(uint8_t x, uint8_t y, char ch, bool invert) {
    return lcd_st7565_char_print(&lcd_cfg, x, y, ch, invert);
}

bool pcb_lcd_string_print(uint8_t x, uint8_t y, const char* str, bool invert) {
    return lcd_st7565_string_print(&lcd_cfg, x, y, str, invert);
}

bool pcb_lcd_dec_small_number_print(uint8_t x, uint8_t y, uint32_t val,
                                    bool invert) {
    return lcd_st7565_dec_small_number_print(&lcd_cfg, x, y, val, invert);
}

bool pcb_lcd_dec_small_number_to_center_print(uint8_t x_start, uint8_t x_end,
                                              uint8_t y, uint32_t val,
                                              bool invert) {
    return lcd_st7565_dec_small_number_to_center_print(&lcd_cfg, x_start, x_end,
                                                       y, val, invert);
}

bool pcb_lcd_hex_small_number_print(uint8_t x, uint8_t y, uint32_t val,
                                    uint8_t num_ch, bool invert) {
    return lcd_st7565_hex_small_number_print(&lcd_cfg, x, y, val, num_ch,
                                             invert);
}

bool pcb_lcd_dec_number_print(uint8_t x, uint8_t y, uint32_t val, bool invert) {
    return lcd_st7565_dec_number_print(&lcd_cfg, x, y, val, invert);
}

bool pcb_lcd_hex_number_print(uint8_t x, uint8_t y, uint32_t val,
                              uint8_t num_ch, bool invert) {
    return lcd_st7565_hex_number_print(&lcd_cfg, x, y, val, num_ch, invert);
}


bool pcb_lcd_string_to_center_print(uint8_t x_start, uint8_t x_end, uint8_t y,
                                    const char* str, bool invert) {
    return lcd_st7565_string_to_center_print(&lcd_cfg, x_start, x_end, y, str,
                                             invert);
}
