#pragma once

#include "app_cfg.h"

#include <stdbool.h>
#include <stdint.h>

bool pcb_lcd_update(uint16_t event);

bool pcb_lcd_is_busy(bool* busy);
bool pcb_lcd_init();
bool pcb_lcd_reset();
bool pcb_lcd_on();
bool pcb_lcd_off();
bool pcb_lcd_send_buffer();
bool pcb_lcd_clear();
bool pcb_lcd_horizontal_line_draw(uint8_t x, uint8_t y, uint8_t len,
                                  bool invert);
bool pcb_lcd_vertical_line_draw(uint8_t x, uint8_t y, uint8_t len, bool invert);
bool pcb_lcd_rectangle_draw(uint8_t x, uint8_t y, uint8_t width, uint8_t height,
                            bool invert);
bool pcb_lcd_rectangle_full_draw(uint8_t x, uint8_t y, uint8_t width,
                                 uint8_t height, bool invert);
bool pcb_lcd_small_one_number_print(uint8_t x, uint8_t y, uint8_t val,
                                    bool invert);
bool pcb_lcd_char_print(uint8_t x, uint8_t y, char ch, bool invert);
bool pcb_lcd_string_print(uint8_t x, uint8_t y, const char* str, bool invert);
bool pcb_lcd_dec_small_number_print(uint8_t x, uint8_t y, uint32_t val,
                                    bool invert);
bool pcb_lcd_dec_small_number_to_center_print(uint8_t x_start, uint8_t x_end,
                                              uint8_t y, uint32_t val,
                                              bool invert);
bool pcb_lcd_hex_small_number_print(uint8_t x, uint8_t y, uint32_t val,
                                    uint8_t num_ch, bool invert);
bool pcb_lcd_dec_number_print(uint8_t x, uint8_t y, uint32_t val, bool invert);
bool pcb_lcd_hex_number_print(uint8_t x, uint8_t y, uint32_t val,
                              uint8_t num_ch, bool invert);
bool pcb_lcd_string_to_center_print(uint8_t x_start, uint8_t x_end, uint8_t y,
                                    const char* str, bool invert);

#define BUTTEN_NUMBER 5

bool pcb_check_buttons_update(uint16_t event);
bool pcb_check_buttons_state_get(bool* clicked);
bool pcb_check_buttons_num_get(uint8_t* button_num);
bool pcb_check_buttons_push_state_get(uint8_t button_num, bool* pushed);

bool pcb_action_joystick_update(uint16_t event);
bool pcb_action_joystick_state_get(bool* clicked);
bool pcb_action_joystick_num_get(uint8_t* button_num);
