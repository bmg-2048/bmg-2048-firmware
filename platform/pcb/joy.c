#include "action_joystick.h"
#include "mc_level.h"

static action_joystick_ram ram = {.final_state    = B_NO,
                                  .captured_state = B_NO,
                                  .down_counter   = 0,
                                  .step           = 0};

static action_joystick_cfg cfg = {.get_x         = mc_adc_x_get,
                                  .get_y         = mc_adc_y_get,
                                  .wait_tick_num = MS_TO_TICK(20),
                                  .ram           = &ram};

bool pcb_action_joystick_update(uint16_t event) {
    return action_joystick_update(&cfg, event);
}

bool pcb_action_joystick_state_get(bool* clicked) {
    return action_joystick_buttons_state_get(&cfg, clicked);
}

bool pcb_action_joystick_num_get(uint8_t* button_num) {
    return action_joystick_buttons_num_get(&cfg, button_num);
}
