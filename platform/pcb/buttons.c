#include "check_buttons.h"
#include "mc_level.h"
#include "pcb_level.h"

static bool pin_state_get(uint8_t pin_number, bool* ret_state) {
    if (pin_number >= BUTTEN_NUMBER) {
        return false;
    }

    switch (pin_number) {
        case B_RIGHT:
            return mc_button_pin_right_state_get(ret_state);
        case B_LEFT:
            return mc_button_pin_left_state_get(ret_state);
        case B_UP:
            return mc_button_pin_up_state_get(ret_state);
        case B_DOWN:
            return mc_button_pin_down_state_get(ret_state);
        case B_JOY:
            return mc_button_pin_stick_state_get(ret_state);
        default:
            return false;
    }
}

static check_buttons_ram check_buts_ram[BUTTEN_NUMBER] = {0};

static const check_buttons_cfg check_buts_cfg = {.pin_state_get = pin_state_get,
                                                 .button_number = BUTTEN_NUMBER,
                                                 .buttons_ram = check_buts_ram,
                                                 .click_time  = MS_TO_TICK(50)};

bool pcb_check_buttons_update(uint16_t event) {
    return check_buttons_update(&check_buts_cfg, event);
}

bool pcb_check_buttons_state_get(bool* clicked) {
    return check_buttons_state_get(&check_buts_cfg, clicked);
}

bool pcb_check_buttons_num_get(uint8_t* button_num) {
    return check_buttons_num_get(&check_buts_cfg, button_num);
}

bool pcb_check_buttons_push_state_get(uint8_t button_num, bool* pushed) {
    return check_buttonspush_state_get(&check_buts_cfg, button_num, pushed);
}