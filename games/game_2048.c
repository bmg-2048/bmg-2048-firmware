#include "game_2048.h"
#include <string.h>
#include "cast_funcs.h"

static void fields_copy(const game_2048_cfg* cfg, uint16_t* buf) {
    memcpy(U8P(buf), U8P(cfg->ram->field_states),
           sizeof(cfg->ram->field_states));
}

static void fields_restore(const game_2048_cfg* cfg, uint16_t* buf) {
    memcpy(U8P(cfg->ram->field_states), U8P(buf),
           sizeof(cfg->ram->field_states));
}

static bool block_print(const game_2048_cfg* cfg, uint8_t x, uint8_t y,
                        uint16_t value) {
    if (!cfg->f->lcd_rectangle_full_draw(U8(x + 1u), U8(y + 1u),
                                         U8(SIZE_BLOCK - 2u),
                                         U8(SIZE_BLOCK - 2u), false)) {
        return false;
    }

    return cfg->f->lcd_dec_small_number_to_center_print(
        U8(x + 2u), U8(U8(x + SIZE_BLOCK) - 2u),
        U8(U8(y + SIZE_BLOCK / 2u) - 2u), value, true);
}

static bool field_print(const game_2048_cfg* cfg) {
    for (uint8_t x = 0u; x < X_ITEM_NUMBER; x++) {
        for (uint8_t y = 0u; y < Y_ITEM_NUMBER; y++) {
            if (cfg->ram->field_states[x][y] != 0u) {
                if (!block_print(cfg,
                                 U8(1u + ((x) ? x * (SIZE_BLOCK - 1u) : 0u)),
                                 U8(1u + ((y) ? y * (SIZE_BLOCK - 1u) : 0u)),
                                 cfg->ram->field_states[x][y])) {
                    return false;
                }
            }
        }
    }

    return true;
}

static bool border_print(const game_2048_cfg* cfg) {
    if (!cfg->f->lcd_horizontal_line_draw(0u, 0u, 128u, false)) {
        return false;
    }
    if (!cfg->f->lcd_horizontal_line_draw(0u, 63u, 128u, false)) {
        return false;
    }
    if (!cfg->f->lcd_vertical_line_draw(0u, 0u, 64u, false)) {
        return false;
    }
    if (!cfg->f->lcd_vertical_line_draw(21u * 5u - 3u, 0u, 64u, false)) {
        return false;
    }
    return cfg->f->lcd_vertical_line_draw(127u, 0u, 64u, false);
}

static const char fail_str[]  = "Game over";
static const char rst_str[]   = "Press joy";
static const char rst_str_1[] = "to restart";

static bool msg_game_over_print(const game_2048_cfg* cfg) {
    uint8_t width  = 21u * 5u - 3u - 5u;
    uint8_t height = 57u;
    if (!cfg->f->lcd_rectangle_full_draw(3u, 3u, width, height, true)) {
        return false;
    }
    if (!cfg->f->lcd_rectangle_draw(3u, 3u, width, height, false)) {
        return false;
    }
    if (!cfg->f->lcd_string_to_center_print(
            5u, U8(5u + width), U8(3u + height / 2u - 12u), fail_str, false)) {
        return false;
    }
    if (!cfg->f->lcd_string_to_center_print(
            5u, U8(5u + width), U8(3u + height / 2u - 4u), rst_str, false)) {
        return false;
    }
    return cfg->f->lcd_string_to_center_print(
        5u, U8(5u + width), U8(3u + height / 2u + 4u), rst_str_1, false);
}

#define X_COUNTER_P   U8(127u - 23u)
#define Y_COUNTER_P_1 U8(2u)
#define Y_COUNTER_P_2 U8(Y_COUNTER_P_1 + 16u)

static bool current_counter_print(const game_2048_cfg* cfg) {
    uint8_t y = Y_COUNTER_P_1;

    if (!cfg->f->lcd_string_print(X_COUNTER_P, y, "Now:", false)) {
        return false;
    }
    y = U8(y + 8u);

    return cfg->f->lcd_hex_small_number_print(
        X_COUNTER_P, y, cfg->ram->current_counter, 5u, false);
}

static bool max_counter_print(const game_2048_cfg* cfg) {
    uint8_t y = Y_COUNTER_P_2;

    if (!cfg->f->lcd_string_print(X_COUNTER_P, y, "Max:", false)) {
        return false;
    }
    y = U8(y + 8u);

    return cfg->f->lcd_hex_small_number_print(
        X_COUNTER_P, y, cfg->eeprom->max_value, 5u, false);
}

static bool x_space_glue_right(const game_2048_cfg* cfg, uint8_t y) {
    int rv = false;

    for (int8_t x = X_ITEM_NUMBER - 1u; x > 0u; x--) {
        if ((cfg->ram->field_states[x - 1u][y] !=
             cfg->ram->field_states[x][y]) &&
            (cfg->ram->field_states[x][y] == 0u)) {
            cfg->ram->field_states[x][y]     = cfg->ram->field_states[x - 1][y];
            cfg->ram->field_states[x - 1][y] = 0;
            rv                               = true;
        }
    }

    return rv;
}

static bool x_space_glue_left(const game_2048_cfg* const cfg, uint8_t y) {
    int rv = false;

    for (int8_t x = 0; x < X_ITEM_NUMBER - 1; x++) {
        if ((cfg->ram->field_states[x][y] !=
             cfg->ram->field_states[x + 1][y]) &&
            (cfg->ram->field_states[x][y] == 0)) {
            cfg->ram->field_states[x][y]     = cfg->ram->field_states[x + 1][y];
            cfg->ram->field_states[x + 1][y] = 0;
            rv                               = true;
        }
    }

    return rv;
}

static bool y_space_glue_up(const game_2048_cfg* const cfg, uint8_t x) {
    int rv = false;

    for (int8_t y = Y_ITEM_NUMBER - 1; y > 0; y--) {
        if ((cfg->ram->field_states[x][y - 1] !=
             cfg->ram->field_states[x][y]) &&
            (cfg->ram->field_states[x][y - 1] == 0)) {
            cfg->ram->field_states[x][y - 1] = cfg->ram->field_states[x][y];
            cfg->ram->field_states[x][y]     = 0;
            rv                               = true;
        }
    }

    return rv;
}

static bool y_space_glue_down(const game_2048_cfg* const cfg, uint8_t x) {
    int rv = false;

    for (int8_t y = 0; y < Y_ITEM_NUMBER - 1; y++) {
        if ((cfg->ram->field_states[x][y + 1] !=
             cfg->ram->field_states[x][y]) &&
            (cfg->ram->field_states[x][y + 1] == 0)) {
            cfg->ram->field_states[x][y + 1] = cfg->ram->field_states[x][y];
            cfg->ram->field_states[x][y]     = 0;
            rv                               = true;
        }
    }

    return rv;
}

static bool x_numbers_glue_right(const game_2048_cfg* const cfg, uint8_t y) {
    int rv = false;

    for (int8_t x = X_ITEM_NUMBER - 1; x > 0; x--) {
        if ((cfg->ram->field_states[x - 1][y] ==
             cfg->ram->field_states[x][y]) &&
            (cfg->ram->field_states[x][y] != 0)) {
            cfg->ram->field_states[x][y] =
                (uint16_t)(cfg->ram->field_states[x][y] << 1u);
            cfg->ram->field_states[x - 1][y] = 0;
            rv                               = true;
            cfg->ram->free_items++;
            x--;
        }
    }

    return rv;
}

static bool x_numbers_glue_left(const game_2048_cfg* const cfg, uint8_t y) {
    int rv = false;

    for (int8_t x = 0; x < X_ITEM_NUMBER - 1; x++) {
        if ((cfg->ram->field_states[x][y] ==
             cfg->ram->field_states[x + 1][y]) &&
            (cfg->ram->field_states[x][y] != 0)) {
            cfg->ram->field_states[x][y] =
                (uint16_t)(cfg->ram->field_states[x][y] << 1u);
            cfg->ram->field_states[x + 1][y] = 0;
            x++;
            cfg->ram->free_items++;
            rv = true;
        }
    }

    return rv;
}

static bool y_numbers_glue_up(const game_2048_cfg* const cfg, uint8_t x) {
    int rv = false;

    for (int8_t y = 0; y < Y_ITEM_NUMBER - 1; y++) {
        if ((cfg->ram->field_states[x][y + 1] ==
             cfg->ram->field_states[x][y]) &&
            (cfg->ram->field_states[x][y + 1] != 0)) {
            cfg->ram->field_states[x][y + 1] =
                (uint16_t)(cfg->ram->field_states[x][y + 1] << 1u);
            cfg->ram->field_states[x][y] = 0;
            y++;
            cfg->ram->free_items++;
            rv = true;
        }
    }

    return rv;
}

static bool y_numbers_glue_down(const game_2048_cfg* const cfg, uint8_t x) {
    int rv = false;

    for (int8_t y = Y_ITEM_NUMBER - 1; y > 0; y--) {
        if ((cfg->ram->field_states[x][y - 1] ==
             cfg->ram->field_states[x][y]) &&
            (cfg->ram->field_states[x][y - 1] != 0)) {
            cfg->ram->field_states[x][U8(U8(y) - 1)] =
                (uint16_t)(cfg->ram->field_states[x][U8(U8(y) - 1u)] << 1u);
            cfg->ram->field_states[x][y] = 0;
            y--;
            cfg->ram->free_items++;
            rv = true;
        }
    }

    return rv;
}

static bool is_not_end_step(const game_2048_cfg* cfg) {
    uint16_t cpy[X_ITEM_NUMBER][Y_ITEM_NUMBER] = {0};
    fields_copy(cfg, (uint16_t*)cpy);
    bool     rv                  = false;
    uint8_t  cpy_free_items      = cfg->ram->free_items;
    uint32_t cpy_current_counter = cfg->ram->current_counter;

    do {
        for (uint8_t x = 0; x < X_ITEM_NUMBER; x++) {
            while (y_space_glue_up(cfg, x) != false) {
                rv = true;
                break;
            }
            rv |= y_numbers_glue_up(cfg, x);
            if (rv == true) {
                break;
            }
            while (y_space_glue_up(cfg, x) != false) {
                rv = true;
                break;
            }
        }

        for (uint8_t x = 0; x < X_ITEM_NUMBER; x++) {
            while (y_space_glue_down(cfg, x) != false) {
                rv = true;
                break;
            }
            rv |= y_numbers_glue_down(cfg, x);
            if (rv == true) {
                break;
            }
            while (y_space_glue_down(cfg, x) != false) {
                rv = true;
                break;
            }
        }

        for (uint8_t y = 0; y < Y_ITEM_NUMBER; y++) {
            while (x_space_glue_left(cfg, y) != false) {
                rv = true;
                break;
            }
            rv |= x_numbers_glue_left(cfg, y);
            if (rv == true) {
                break;
            }
            while (x_space_glue_left(cfg, y) != false) {
                rv = true;
                break;
            }
        }

        for (uint8_t y = 0; y < Y_ITEM_NUMBER; y++) {
            while (x_space_glue_right(cfg, y) != false) {
                rv = true;
                break;
            }
            rv |= x_numbers_glue_right(cfg, y);
            if (rv == true) {
                break;
            }
            while (x_space_glue_right(cfg, y) != false) {
                rv = true;
                break;
            }
        }

        break;
    } while (true);

    if (rv) {
        fields_restore(cfg, (uint16_t*)cpy);
        cfg->ram->free_items      = cpy_free_items;
        cfg->ram->current_counter = cpy_current_counter;
    }

    return rv;
}

bool swap_field(const game_2048_cfg* cfg, uint8_t direction) {
    bool rv = false;

    switch (direction) {
        case B_UP:
            for (uint8_t x = 0; x < X_ITEM_NUMBER; x++) {
                while (y_space_glue_up(cfg, x) != false) {
                    rv = true;
                }
                rv |= y_numbers_glue_up(cfg, x);
                while (y_space_glue_up(cfg, x) != false) {
                    rv = true;
                }
            }
            break;

        case B_DOWN:
            for (uint8_t x = 0; x < X_ITEM_NUMBER; x++) {
                while (y_space_glue_down(cfg, x) != false) {
                    rv = true;
                }
                rv |= y_numbers_glue_down(cfg, x);
                while (y_space_glue_down(cfg, x) != false) {
                    rv = true;
                }
            }
            break;

        case B_LEFT:
            for (uint8_t y = 0; y < Y_ITEM_NUMBER; y++) {
                while (x_space_glue_left(cfg, y) != false) {
                    rv = true;
                }
                rv |= x_numbers_glue_left(cfg, y);
                while (x_space_glue_left(cfg, y) != false) {
                    rv = true;
                }
            }
            break;

        case B_RIGHT:
            for (uint8_t y = 0; y < Y_ITEM_NUMBER; y++) {
                while (x_space_glue_right(cfg, y) != false) {
                    rv = true;
                }
                rv |= x_numbers_glue_right(cfg, y);
                while (x_space_glue_right(cfg, y) != false) {
                    rv = true;
                }
            }
            break;

        default:
            break;
    }

    return rv;
}

void add_new_item(const game_2048_cfg* const cfg) {
    uint64_t time        = cfg->f->systick_counter_get();
    uint8_t  item_number = U8(time % cfg->ram->free_items);

    for (uint8_t x = 0; x < X_ITEM_NUMBER; x++) {
        for (uint8_t y = 0; y < Y_ITEM_NUMBER; y++) {
            if (cfg->ram->field_states[x][y] == 0) {
                if (item_number != 0) {
                    item_number--;
                } else {
                    uint8_t value                = (time % 3) ? 4 : 2;
                    cfg->ram->field_states[x][y] = value;
                    cfg->ram->current_counter += value;
                    cfg->ram->free_items--;
                    return;
                }
            }
        }
    }
}

void field_clear(const game_2048_cfg* const cfg) {
    memset(cfg->ram->field_states, 0, sizeof(cfg->ram->field_states));
}

static bool start_window_print(const game_2048_cfg* const cfg) {
    if (!cfg->f->lcd_clear()) {
        return false;
    }
    if (!border_print(cfg)) {
        return false;
    }
    if (!field_print(cfg)) {
        return false;
    }
    if (!current_counter_print(cfg)) {
        return false;
    }
    if (!max_counter_print(cfg)) {
        return false;
    }
    return true;
}

void game_2048_init(const game_2048_cfg* cfg) {
    if (!cfg) {
        while (true)
            ;
    }

    memset(cfg->ram, 0, sizeof(*cfg->ram));
}

#define STEP_GAME_INIT           0
#define STEP_CFG_CHECK           1
#define STEP_CFG_RESTORE_WAIT    2
#define STEP_WINDOW_PRINT        3
#define STEP_END_CHECK           4
#define STEP_KEY_JOY_ACTION_WAIT 5
#define STEP_NEW_ITEM_ADD        6
#define STEP_END_PRINT           7
#define STEP_CFG_WRITING_START   8
#define STEP_CFG_SAVING_WAIT     9
#define STEP_RESTART_WAIT        10

static void step_game_init(const game_2048_cfg* cfg) {
    bool buf_bool = false;

    if (!cfg->f->lcd_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    field_clear(cfg);
    cfg->ram->field_states[0][0] = 2;
    cfg->ram->free_items         = X_ITEM_NUMBER * Y_ITEM_NUMBER - 1;
    cfg->ram->current_counter    = 2;
    cfg->ram->step++;
}

static void step_cfg_check(const game_2048_cfg* cfg) {
    bool buf_bool = false;
    if (!cfg->f->cfg_controller_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    if (!cfg->f->cfg_controller_is_cfg_valid(
            cfg->eeprom_word_num_start,
            sizeof(game_2048_eeprom) / sizeof(uint32_t), &buf_bool)) {
        return;
    }
    if (buf_bool) {
        if (!cfg->f->cfg_controller_cfg_read(
                (uint32_t*)cfg->eeprom,
                sizeof(game_2048_eeprom) / sizeof(uint32_t),
                cfg->eeprom_word_num_start)) {
            return;
        }
        cfg->ram->step = STEP_WINDOW_PRINT;
    } else {
        memset(cfg->eeprom, 0, sizeof(*cfg->eeprom));
        if (!cfg->f->cfg_controller_cfg_start_to_save(
                (uint32_t*)cfg->eeprom,
                sizeof(game_2048_eeprom) / sizeof(uint32_t),
                cfg->eeprom_word_num_start)) {
            return;
        }
        cfg->ram->step++;
    }
}

static void step_cfg_restore_wait(const game_2048_cfg* cfg) {
    bool buf_bool = false;
    if (!cfg->f->cfg_controller_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    cfg->ram->step = STEP_WINDOW_PRINT;
}

static void step_window_print(const game_2048_cfg* cfg) {
    bool buf_bool = false;
    if (!cfg->f->lcd_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    if (!start_window_print(cfg)) {
        return;
    }
    if (!cfg->f->lcd_send_buffer()) {
        return;
    }
    cfg->ram->step = STEP_END_CHECK;
}

static void step_end_check(const game_2048_cfg* cfg) {
    bool buf_bool = false;
    if (!cfg->f->lcd_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    if (!is_not_end_step(cfg)) {
        cfg->ram->step = STEP_END_PRINT;
        return;
    }
    cfg->ram->step = STEP_KEY_JOY_ACTION_WAIT;
}

static void step_key_joy_action_wait(const game_2048_cfg* cfg) {
    bool    buf_bool   = false;
    uint8_t button_num = 0;

    if (!cfg->f->check_buttons_state_get(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        if (!cfg->f->check_buttons_num_get(&button_num)) {
            return;
        }

        if (!swap_field(cfg, button_num)) {
            return;
        }
        cfg->ram->step = STEP_NEW_ITEM_ADD;
        return;
    }
    if (!cfg->f->action_joystick_state_get(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        if (!cfg->f->action_joystick_num_get(&button_num)) {
            return;
        }

        if (!swap_field(cfg, button_num)) {
            return;
        }
        cfg->ram->step = STEP_NEW_ITEM_ADD;
    }
}

static void step_new_item_add(const game_2048_cfg* cfg) {
    add_new_item(cfg);
    cfg->ram->step = STEP_WINDOW_PRINT;
}

static void step_end_print(const game_2048_cfg* cfg) {
    msg_game_over_print(cfg);
    if (!cfg->f->lcd_send_buffer()) {
        return;
    }
    if (cfg->ram->current_counter > cfg->eeprom->max_value) {
        cfg->eeprom->max_value = cfg->ram->current_counter;
        cfg->ram->step++;
        return;
    }
    cfg->ram->step = STEP_RESTART_WAIT;
}

static void step_cfg_writing_start(const game_2048_cfg* cfg) {
    bool buf_bool = false;

    if (!cfg->f->cfg_controller_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }

    if (!cfg->f->cfg_controller_cfg_start_to_save(
            (uint32_t*)cfg->eeprom, sizeof(game_2048_eeprom) / sizeof(uint32_t),
            cfg->eeprom_word_num_start)) {
        return;
    }
    cfg->ram->step = STEP_CFG_SAVING_WAIT;
}

static void step_cfg_saving_wait(const game_2048_cfg* cfg) {
    bool buf_bool = false;

    if (!cfg->f->cfg_controller_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    cfg->ram->step = STEP_RESTART_WAIT;
}

static void step_restart_wait(const game_2048_cfg* cfg) {
    bool    buf_bool   = false;
    uint8_t button_num = 0;

    if (!cfg->f->lcd_is_busy(&buf_bool)) {
        return;
    }
    if (buf_bool) {
        return;
    }
    if (!cfg->f->check_buttons_state_get(&buf_bool)) {
        return;
    }
    if (!buf_bool) {
        return;
    }
    if (!cfg->f->check_buttons_num_get(&button_num)) {
        return;
    }
    if (button_num != B_JOY) {
        return;
    }
    cfg->ram->step = STEP_GAME_INIT;
}

typedef void (*step_f)(const game_2048_cfg* cfg);

static const step_f steps[11] = {
    step_game_init,       step_cfg_check,   step_cfg_restore_wait,
    step_window_print,    step_end_check,   step_key_joy_action_wait,
    step_new_item_add,    step_end_print,   step_cfg_writing_start,
    step_cfg_saving_wait, step_restart_wait};

void game_2048_update(const game_2048_cfg* cfg, uint16_t event) {
    (void)event;

    if (!cfg) {
        while (true)
            ;
    }

    steps[cfg->ram->step](cfg);
}
