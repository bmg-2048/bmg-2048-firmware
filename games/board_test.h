#pragma once

#include "app_cfg.h"
#include "app_ref_table.h"

typedef struct {
    uint8_t step;
    uint8_t x;
    uint8_t y;
    bool    button_old_state[5];
} board_test_ram;

typedef struct {
    const app_func_ref_table* f;
    board_test_ram*           ram;
} board_test_cfg;

void board_test_init(const board_test_cfg* cfg);
void board_test_update(const board_test_cfg* cfg, uint16_t event);
