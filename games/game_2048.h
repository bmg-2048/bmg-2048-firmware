#pragma once

#include "app_cfg.h"
#include "app_ref_table.h"

#define SIZE_BLOCK    21
#define X_ITEM_NUMBER 5
#define Y_ITEM_NUMBER 3

typedef struct {
    uint32_t max_value;
} game_2048_eeprom;

typedef struct {
    uint8_t  step;
    uint16_t field_states[X_ITEM_NUMBER][Y_ITEM_NUMBER];
    uint8_t  free_items;
    uint32_t current_counter;
    uint8_t  state;
} game_2048_ram;

typedef struct {
    const app_func_ref_table* f;
    game_2048_ram*            ram;
    game_2048_eeprom*         eeprom;
    uint8_t                   eeprom_word_num_start;
} game_2048_cfg;

void game_2048_init(const game_2048_cfg* cfg);
void game_2048_update(const game_2048_cfg* cfg, uint16_t event);
