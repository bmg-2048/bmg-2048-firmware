#include "board_test.h"
#include <string.h>

#define B_W       19
#define B_X_START 67
#define XY_TEXT_Y 48

static void write_buttons(const board_test_cfg* cfg) {
    (void)cfg;
    bool pushed = false;

    if (!cfg->f->check_buttons_push_state_get(B_LEFT, &pushed)) {
        return;
    }
    if (pushed != cfg->ram->button_old_state[B_LEFT]) {
        if (!cfg->f->lcd_rectangle_full_draw(2 + B_X_START, 2 + 2 + (B_W + 1),
                                             B_W - 4, B_W - 4, !pushed)) {
            return;
        }
        cfg->ram->button_old_state[B_LEFT] = pushed;
    }

    if (!cfg->f->check_buttons_push_state_get(B_JOY, &pushed)) {
        return;
    }
    if (pushed != cfg->ram->button_old_state[B_JOY]) {
        if (!cfg->f->lcd_rectangle_full_draw(2 + B_X_START + (B_W + 1) * 1,
                                             2 + 2 + (B_W + 1), B_W - 4,
                                             B_W - 4, !pushed)) {
            return;
        }
        cfg->ram->button_old_state[B_JOY] = pushed;
    }

    if (!cfg->f->check_buttons_push_state_get(B_RIGHT, &pushed)) {
        return;
    }
    if (pushed != cfg->ram->button_old_state[B_RIGHT]) {
        if (!cfg->f->lcd_rectangle_full_draw(2 + B_X_START + (B_W + 1) * 2,
                                             2 + 2 + (B_W + 1), B_W - 4,
                                             B_W - 4, !pushed)) {
            return;
        }
        cfg->ram->button_old_state[B_RIGHT] = pushed;
    }

    if (!cfg->f->check_buttons_push_state_get(B_UP, &pushed)) {
        return;
    }
    if (pushed != cfg->ram->button_old_state[B_UP]) {
        if (!cfg->f->lcd_rectangle_full_draw(2 + B_X_START + (B_W + 1) * 1, 4,
                                             B_W - 4, B_W - 4, !pushed)) {
            return;
        }
        cfg->ram->button_old_state[B_UP] = pushed;
    }

    if (!cfg->f->check_buttons_push_state_get(B_DOWN, &pushed)) {
        return;
    }
    if (pushed != cfg->ram->button_old_state[B_DOWN]) {
        if (!cfg->f->lcd_rectangle_full_draw(2 + B_X_START + (B_W + 1) * 1,
                                             4 + (B_W + 1) * 2, B_W - 4,
                                             B_W - 4, !pushed)) {
            return;
        }
        cfg->ram->button_old_state[B_DOWN] = pushed;
    }
    return;
}

void board_test_init(const board_test_cfg* const cfg) {
    if (!cfg) {
        while (true)
            ;
    }

    memset(cfg->ram, 0, sizeof(*cfg->ram));
}

#define STEP_GAME_INIT           0
#define STEP_KEY_JOY_ACTION_WAIT 1

void board_test_update(const board_test_cfg* cfg, uint16_t event) {
    (void)event;

    if (!cfg) {
        while (true)
            ;
    }

    bool lcd_busy = false;

    switch (cfg->ram->step) {
        case STEP_GAME_INIT:
            if (!cfg->f->lcd_is_busy(&lcd_busy)) {
                return;
            }
            if (lcd_busy) {
                return;
            }
            if (!cfg->f->lcd_rectangle_draw(0, 0, 128, 64, false)) {
                return;
            }
            if (!cfg->f->lcd_rectangle_draw(2, 2, 31 + 10 + 2 * 2,
                                            31 + 10 + 2 * 2, false)) {
                return;
            }
            if (!cfg->f->lcd_string_print(2, XY_TEXT_Y, "X:", false)) {
                return;
            }
            if (!cfg->f->lcd_string_print(2 + 4 * 6, XY_TEXT_Y, "Y:", false)) {
                return;
            }
            if (!cfg->f->lcd_rectangle_draw(B_X_START + (B_W + 1) * 1, 2 + 0,
                                            B_W, B_W, false)) {
                return;
            }
            if (!cfg->f->lcd_rectangle_draw(B_X_START, 2 + (B_W + 1), B_W, B_W,
                                            false)) {
                return;
            }
            if (!cfg->f->lcd_rectangle_draw(B_X_START + (B_W + 1) * 1,
                                            2 + B_W + 1, B_W, B_W, false)) {
                return;
            }
            if (!cfg->f->lcd_rectangle_draw(B_X_START + (B_W + 1) * 2,
                                            2 + B_W + 1, B_W, B_W, false)) {
                return;
            }
            if (!cfg->f->lcd_rectangle_draw(B_X_START + (B_W + 1) * 1,
                                            2 + (B_W + 1) * 2, B_W, B_W,
                                            false)) {
                return;
            }
            cfg->ram->step++;
            break;

        case STEP_KEY_JOY_ACTION_WAIT:
            if (!cfg->f->lcd_is_busy(&lcd_busy)) {
                return;
            }
            if (lcd_busy) {
                return;
            }

            if (!cfg->f->lcd_rectangle_full_draw((uint8_t)(cfg->ram->x + 4u),
                                                 (uint8_t)(cfg->ram->y + 4u),
                                                 10u, 10u, true)) {
                return;
            }

            cfg->ram->x = (uint8_t)(cfg->f->adc_x_get() / 128u);
            cfg->ram->y = (uint8_t)(cfg->f->adc_y_get() / 128u);

            if (!cfg->f->lcd_rectangle_full_draw((uint8_t)(cfg->ram->x + 4u),
                                                 (uint8_t)(cfg->ram->y + 4u),
                                                 10u, 10u, false)) {
                return;
            }

            if (!cfg->f->lcd_rectangle_full_draw(6 * 2, XY_TEXT_Y, 6 * 2, 7,
                                                 true)) {
                return;
            }
            if (!cfg->f->lcd_dec_number_print(6 * 2, XY_TEXT_Y, cfg->ram->x,
                                              false)) {
                return;
            }
            if (!cfg->f->lcd_rectangle_full_draw(6 * 6, XY_TEXT_Y, 6 * 2, 7,
                                                 true)) {
                return;
            }
            if (!cfg->f->lcd_dec_number_print(6 * 6, XY_TEXT_Y, cfg->ram->y,
                                              false)) {
                return;
            }
            write_buttons(cfg);
            if (!cfg->f->lcd_send_buffer()) {
                return;
            }
            break;
    }
}
